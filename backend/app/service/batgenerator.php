<?php

namespace Service;

class BatGenerator extends \Prefab
{
    protected
        //$zip,
        //$ujian,
        $zipname;

    public function generate(\Model\Ujian\Exam $ujian)
    {
        $zip = new \ZipArchive();
        $zipname = $this->tempFileGenerator(".zip");
        //$this->zip = $zip;
        $ujian->participants->orderBy('posisi asc');
        //$this->ujian = $ujian;

        \F3::set("data.ujian", $ujian);
        $zip->open($zipname, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
        $zip->addFromString('01-mkdir.bat', \View\Bat::render("lab-mkdir.bat", "01-mkdir.log"));
        $zip->addFromString('02-copy.bat', \View\Bat::render("lab-copy.bat", "02-copy.log"));
        $zip->addFromString('03-takeown.bat', \View\Bat::render("lab-takeown.bat", "03-takeown.log"));
        $zip->addFromString('WARNING-special--takeown-full-ujian.bat', \View\Bat::render("special-ujian-fulltakeown.bat", "04-special-takeown.log"));
        $zip->close();

        return $zipname;
    }

    public function generateMigration(array $migrationLists, $exam)
    {
        $zip = new \ZipArchive();
        $zipname = $this->tempFileGenerator(".zip");
        $this->zipname = $zipname;

        // Limit exam lists.
        $participants = $exam->participants;
        $exam->participants = array_map(function ($d) {
            return $d['p'];
        }, $migrationLists);

        //Updating $exam->participants record to only participants in $migrationList
        //There is behavior such as $exam->participants variable is only an array consisting of number Example: Array([0]=>2, [1]=>3)
        //The $exam->participants is not converted to Participant object like what stated in library documentation. 
        //Thus, both mkdir.bat and copy.bat result in a script with empty participant location and username
        $exam = $exam->update();

        \F3::set("data.ujian", $exam);
        
        \F3::set("data.migrations", $migrationLists);
        $zip->open($zipname, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
        $zip->addFromString('00-migrate-folders.bat', \View\Bat::render("lab-migrate.bat"));

        $zip->addFromString('01-mkdir.bat', \View\Bat::render("lab-mkdir.bat", "01-mkdir.log"));
        $zip->addFromString('02-copy.bat', \View\Bat::render("lab-copy.bat", "02-copy.log"));
        //Adding takeown for computer where the participants moved into
        $zip->addFromString('03-takeown.bat', \View\Bat::render("lab-takeown.bat", "03-takeown.log"));
        $zip->close();

        $exam->participants = $participants;
        //Updating $exam->participants record to all participants in $participants (revert back to the previous participants)
        $exam = $exam->update();
        return $zipname;
    }

    public function clean_zip()
    {
        \unlink($this->zipname);
    }

    public function tempFileGenerator($prefix = ".tmp", $length = 10, $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz')
    {
        $temp = "";
        while ($length-- > 0) {
            $temp .= $chars[\rand(0, \strlen($chars))];
        }
        return \F3::get('TEMP') . '/' . $temp . $prefix;
    }
}
