<?php

namespace Migration;

use Exception;
use Model\System\User;

/**
 * Migration Example
 * Please read more documentation on https://github.com/chez14/f3-ilgar
 */
class OnlineExamPermissionData extends \Chez14\Ilgar\MigrationPacket
{
    public function on_migrate()
    {
        $permissions = [
            "lecturer" => [
                "manage-ujian-answerslot-online" => \Model\System\AclItem::ALL,
                "manage-ujian-exam-online" => \Model\System\AclItem::ALL,
                "manage-ujian-lecture" => \Model\System\AclItem::READ,
                "manage-ujian-lectureperiod" => \Model\System\AclItem::READ,
                "manage-ujian-participant-online" => \Model\System\AclItem::ALL,
                "manage-ujian-submission-online" => \Model\System\AclItem::READ,
                "manage-ujian-resource-online" => \Model\System\AclItem::ALL,
                "manage-system-user-online" => \Model\System\AclItem::READ,
                "manage-system-acl" => \Model\System\AclItem::NONE,
                "manage-ujian-notification" => \Model\System\AclItem::ALL,
                "manage-ujian-notification-online" => \Model\System\AclItem::ALL,
                "manage-ujian-exam-online-report"=> \Model\System\AclItem::ALL ,
            ],
            "student" => [
                "manage-ujian-submission-online" => \Model\System\AclItem::READ | \Model\System\AclItem::UPDATE,
                "manage-ujian-resource-online" => \Model\System\AclItem::NONE,
                "manage-ujian-exam-online" => \Model\System\AclItem::NONE,
            ],
            "superuser" => [
                "manage-ujian-answerslot-online" => \Model\System\AclItem::ALL,
                "manage-ujian-exam-online" => \Model\System\AclItem::ALL,
                "manage-ujian-lecture" => \Model\System\AclItem::ALL,
                "manage-ujian-lectureperiod" => \Model\System\AclItem::ALL,
                "manage-ujian-participant-online" => \Model\System\AclItem::ALL,
                "manage-ujian-submission-online" => \Model\System\AclItem::ALL,
                "manage-system-user-online" => \Model\System\AclItem::ALL,
                "manage-ujian-resource-online" => \Model\System\AclItem::ALL,
                "manage-system-acl" => \Model\System\AclItem::NONE,
                "manage-system-aclitem" => \Model\System\AclItem::READ,
                "manage-ujian-notification" => \Model\System\AclItem::ALL,
                "manage-ujian-exam-online-report" => \Model\System\AclItem::ALL,
            ]
        ];

        foreach ($permissions as $name => $r) {
            $acl = new \Model\System\Acl();
            $acl->name = $name;
            $acl->save();
            $acls[] = $acl;
            foreach ($r as $codename => $permission) {
                $acli = new \Model\System\AclItem();
                $acli->codename = $codename;
                $acli->permission = $permission;
                $acli->acl = $acl;
                $acli->save();
            }
        }
    }

    public function on_failed(Exception $e)
    {
    }
}
