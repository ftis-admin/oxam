<?php

namespace Migration;

use Exception;
use Ramsey\Uuid\Nonstandard\Uuid;

class BasicDataLecturer extends \Chez14\Ilgar\MigrationPacket
{
    public function on_migrate()
    {
        $lecturerACL = new \Model\System\Acl();

        $lecturerACL->load(["name LIKE ?", "lecturer"]);


        $lecturers = [
            [
                "uuid" => Uuid::uuid4(),
                "username" => "dosen1",
                "password" => "pass_dosen_1",
                "email" => "dosen1@ftis.unpar",
                "acl" => $lecturerACL->dry() ? NULL : $lecturerACL->id,
            ], [
                "uuid" => Uuid::uuid4(),
                "username" => "anung",
                "email" => "anung@unpar.ac.id",
                "acl" => $lecturerACL->dry() ? NULL : $lecturerACL->id,
            ],

        ];
        $noLDAPLecturer = new \Model\System\OnlineUser();
        $noLDAPLecturer->copyfrom($lecturers[0]);
        $noLDAPLecturer->save();

        $ldapLecturer = new \Model\System\OnlineUser();
        $ldapLecturer->copyfrom($lecturers[1]);
        $ldapLecturer->save();
    }

    public function on_failed(Exception $e)
    {
    }
}
