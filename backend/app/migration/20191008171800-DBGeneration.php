<?php

namespace Migration;

/**
 * Migration Example
 * Please read more documentation on https://github.com/chez14/f3-ilgar
 */
class DBGeneration extends \Chez14\Ilgar\MigrationPacket
{
    public function on_migrate()
    {
        $f3 = \F3::instance();

        // making all those models
        \Model\System\User::setup();
        \Model\System\Acl::setup();
        \Model\System\AclItem::setup();

        \Model\System\OnlineUser::setup();  //Table for online user student and lecturer
       
        #Online Exam DB Generation
        \Model\Ujian\Online\ExamResource::setup();
        \Model\Ujian\Online\ResourceToken::setup();
        \Model\Ujian\Online\SubmissionOnline::setup();
        \Model\Ujian\Online\AnswerSlotOnline::setup();
        \Model\Ujian\Online\NotificationOnline::setup();

        \Model\Ujian\Computer::setup();
        \Model\Ujian\Location::setup();
        \Model\Ujian\Lecture::setup();
        \Model\Ujian\LecturePeriod::setup();
        \Model\Ujian\Exam::setup();
        \Model\Ujian\AnswerSlot::setup();
        \Model\Ujian\Participant::setup();
        \Model\Ujian\Submission::setup();
    }

    public function on_failed(\Exception $e)
    {
        echo $e;
    }
}
