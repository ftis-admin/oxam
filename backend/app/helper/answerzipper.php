<?php

namespace Helper;

use Controller\Api\ManageOnline\AnswerSlot;
use Model\System\OnlineUser;
use Model\Ujian\Exam;
use Model\Ujian\Online\AnswerSlotOnline;
use Model\Ujian\Online\SubmissionOnline;
use Model\Ujian\Submission;
use Model\Error;

class AnswerZipper extends \Prefab
{
    public static function zipExam(Exam $exam): string
    {
        $submission = new Submission();
        $submission->has("answer_slot", ["deleted_on = ?", null]);
        $submission->has("participant.exam", ["id=?", $exam->_id]);
        $submissions = $submission->find();

        if (!is_iterable($submissions)) {
            $submissions = [];
        }

        // starting to packing things.
        $zip = new \ZipArchive();
        $zipname = \F3::get('TEMP') . DIRECTORY_SEPARATOR . $exam->exam_uniqcode . ".zip";
        $zip->open($zipname, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

        $zip->addFromString("_examInfo.json", json_encode($exam->cast()));

        foreach ($submissions as $sub) {
            $foldername = $sub->participant->username;
            $foldername .= DIRECTORY_SEPARATOR . $sub->answer_slot->simulateFormat($sub->participant);

            $zip->addFromString($foldername, file_get_contents($sub->getFullPath()));
        }
        $zip->close();

        return $zipname;
    }

    public static function zipExamOnline(Exam $exam): string
    {
        //Get online participants
        $participantIDs = $exam->cast()["online_participants"];
        $participants = [];

        if ($participantIDs && is_iterable($participantIDs)) {
            //Map partipants to object
            foreach ($participantIDs as $par) {
                $onlineUser = new OnlineUser();
                $onlineUser->has("participating_exams", ["id = ?", $exam->id]);
                $onlineUser->load(["uuid LIKE ?", $par["uuid"]]);

                $participants[] = $onlineUser->cast(null, 2);
            }

            // starting to packing things.
            $zip = new \ZipArchive();
            $zipname = \F3::get('TEMP') . DIRECTORY_SEPARATOR . $exam->exam_uniqcode . ".zip";
            $zip->open($zipname, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

            $examInfo = $exam->cast();
            unset($examInfo["notification"]);
            unset($examInfo["exam_report"]);

            $zip->addFromString("_examInfo.json", json_encode($examInfo));
            // $zip->addFromString("_examInfo.json", json_encode($participants));


            //Get submission of each online user
            foreach ($participantIDs as $par) {

                //Get online user
                $onlineUser = new OnlineUser();
                $onlineUser->has("participating_exams", ["id = ?", $exam->id]);
                $onlineUser->load(["uuid LIKE ?", $par["uuid"]]);

                if ($onlineUser->loaded()) {
                    $foldername = $onlineUser->username;

                    $submissions = $onlineUser->cast(null, 2);

                    //There is no submission for this user
                    if (!$submissions || !$submissions["exam_submission"]) {
                        $submissions = [];
                    } else {
                        $submissions = $submissions["exam_submission"];
                    }

                    foreach ($submissions as $sub) {
                        $submission = new SubmissionOnline();
                        $submission->has("answer_slot", ["deleted_on = ?", null]);
                        $submission->load(["id = ?", $sub["_id"]]);

                        if ($submission->loaded()) {
                            $foldername .= DIRECTORY_SEPARATOR . $submission->answer_slot->simulateFormat($onlineUser);
                            $zip->addFromString($foldername, file_get_contents($exam->getFullPath() . DIRECTORY_SEPARATOR . $submission->stored_filename));
                        }
                    }
                }
            }
            $zip->close();

            return $zipname;
        } else {
            throw new Error("Invalid number of participant", "This exam has no participant!", "X400", "Exception", 400);
        }
    }


    public static function getProperFilename(Exam $exam, bool $withDate = true): string
    {
        $filename = $exam->lecture->lecture_code . " " . $exam->lecture->name;
        if ($exam->shift) {
            $filename .= " Shift " . $exam->shift;
        }

        if ($withDate) {
            $filename .= " -- " . date("Y-m-d");
        }

        return $filename;
    }
}
