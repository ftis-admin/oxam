<?php

namespace Model\System;

use Adldap\Models\Model;
use Error;
use DateInterval;
use DateTimeImmutable;
use Ramsey\Uuid\Uuid;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\ValidationData;
use Lcobucci\JWT\Signer\Keychain;
use Lcobucci\JWT\Signer\Rsa\Sha256;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;


class OnlineUser extends \Model\ModelBase
{
    protected
        $fieldConf = array(
            "uuid" => [
                'index' => true,
                'type' => \DB\SQL\Schema::DT_TEXT,
                'unique' => true,
                'nullable' => false,
                '_copyable' => true
            ],
            "username" => [
                'type' => \DB\SQL\Schema::DT_TEXT,
                '_copyable' => true
            ],
            "display_name" => [
                'type' => \DB\SQL\Schema::DT_TEXT,
                '_copyable' => true
            ],
            "password" => [
                'type' => \DB\SQL\Schema::DT_TEXT,
                'nullable' => true,
                '_copyable' => true
            ], "email" => [
                'type' => \DB\SQL\Schema::DT_TEXT,
                '_copyable' => true
            ],
            "acl" => [
                'belongs-to-one' => '\Model\System\Acl',
                '_copyable' => true
            ],

            /** 
             * RELATIONSHIPS
             * */

            "exams" => [
                '_copyable' => false,
                'has-many' => ['\Model\Ujian\Exam', "lecturers", "ujian_online_lecturer_exam"],
            ],

            'participating_exams' => [
                'nullable' => true,
                '_copyable' => false,
                'has-many' => ['\Model\Ujian\Exam', "online_participants", "ujian_online_participant"]
            ],

            'exam_submission' => [
                'nullable' => true,
                '_copyable' => true,
                'has-many' => ['\Model\Ujian\Online\SubmissionOnline', 'submission_owner'],

            ],

            'notifications' => [
                '_copyable' => true,
                'nullable' => true,
                'has-many' => array("\Model\Ujian\Online\NotificationOnline", "participants", "ujian_notif_aud_online")
            ],

            'deleted_on' => [
                'type' => \DB\SQL\Schema::DT_DATETIME,
                'nullable' => true,
                'index' => false,
                'unique' => false,
            ],

            'created_on' => [
                'type' => \DB\SQL\Schema::DT_DATETIME,
                'nullable' => true,
                'index' => false,
                'unique' => false,
            ],
            'updated_on' => [
                'type' => \DB\SQL\Schema::DT_DATETIME,
                'nullable' => true,
                'index' => false,
                'unique' => false,
            ],
        ),
        $db = 'DB',
        $table = 'system_online_user';

    public const
        E_GENERIC = "Kombinasi User dan Password tidak ditemukan, coba lagi";

    public function __construct()
    {
        parent::__construct();

        parent::virtual('xxyyy', function ($x) {
            if ($x->acl->name == "student") {
                return substr($x->username, 1);
            } else {
                return "";
            }
        });

        parent::virtual('display_name', function ($umu) {
            if (!\F3::get('dev_setting.query_ldap'))
                return null;

            $username = $umu->username;
            $prov = \F3::get('LDAP.provider');
            $account = $prov->search()->users()->findBy('sAMAccountName', $username);
            if (!$account)
                return $umu->display_name;
            return mb_convert_case($account->getFirstName() . " " . $account->getLastName(), MB_CASE_TITLE);
        });
    }

    public function set_password($pass)
    {
        return ($pass === null) ? $pass : password_hash($pass, CRYPT_BLOWFISH);
    }

    public function set_deleted_on($date)
    {
        return date("Y-m-d H:i:s", $date);
    }

    public function set_created_on($date)
    {
        return date("Y-m-d H:i:s", $date);
    }

    public function set_updated_on($date)
    {
        return date("Y-m-d H:i:s", $date);
    }

    public function save()
    {
        if (!$this->created_on)
            $this->created_on = time();
        $this->updated_on = time();
        return parent::save();
    }

    /**
     * Get any active participant. Will return either active, upcoming, or null.
     *
     * @param string $ip IP of the computer to be queried default to autodetect.
     * @param integer $upcomingTimeMinutes How far the upcoming exam should be fetched?
     * @return Participant|null
     */
    public static function loginLDAP($username, $password)
    {
        $f3 = \Base::instance();

        $onlineUserInstance = new self;
        $logger = $onlineUserInstance->getLoggerInstance();
        $logger->notice("Login attempt from " . $f3->IP. " for user " . $username);

        //If the query_ldap value true or remain true after LDAP connect in
        //config.php it means connection to LDAP service is a success.
        if (\F3::get('dev_setting.query_ldap')) {
            $provider = $f3->get('LDAP.provider');

            //Check provider is not null
            if ($provider) {
                //Attempting to authenticate using username and password
                if ($provider->auth()->attempt($username, $password)) {
                    //Username and password combination is valid

                    $online_user = new self;

                    //Skipping password and id fields as output
                    $online_user->fields(["password", "id"], true);
                    //Check if the username already exist in database
                    // $online_user->load(["username LIKE ?", $username]);
                    $online_user->load(["username LIKE ?", $username]);

                    if ($online_user->dry()) {
                        //Adding username to Database
                        $online_user->username = $username;
                        $online_user->uuid = Uuid::uuid4();

                        //Set ACL level of user based on group membership on Active Directory
                        $userLDAPDetail = $provider->search()->findBy('samaccountname', $username);
                        $membership = $userLDAPDetail->getAttribute("memberof");
                        $aclName = "default";

                        if ($membership && sizeof($membership)) {
                            foreach ($membership as $memberof) {
                                if (strpos($memberof, "Students")) {
                                    $aclName = "student";
                                }
                                if (strpos($memberof, "Dosen")) {
                                    $aclName = "lecturer";
                                }
                                if (strpos($memberof, "Admins")) {
                                    $aclName = "admin-online";
                                }
                            }
                        }

                        $acl = new \Model\System\Acl();
                        $acl->load(["name LIKE ?", $aclName]);

                        //Set to null if ACL mode is dry
                        if ($acl->loaded()) {
                            $online_user->acl = $acl->id;
                        } else {
                            $online_user->acl = NULL;
                        }

                        //Checking if mail is exist, then add it to user entry in database
                        if ($email = $userLDAPDetail->getAttribute('mail', 0)) {
                            $online_user->email = $email;
                        }

                        // //Check if display name exist, then add it to user entry in database
                        // if ($displayName = $userLDAPDetail->getAttribute('displayname', 0)) {
                        //     $online_user->display_name = $displayName;
                        // }

                        //Saving the data to database
                        $online_user->save();

                        //Reload it, because somehow after the save the model will still be dry
                        $online_user->load(["username LIKE ?", $username]);
                    }
                    return $online_user;
                } else {
                    return false;
                }
            } else {
                throw new  Error("Provider is null", 500, null);
            }
        } else {
            throw new  Error("Query LDAP is disabled", 500, null);
        }
        return false;
    }


    public function generateToken($as = "basic", $iploginId = null)
    {
        if ($this->dry()) {
            throw new \Exception('Model is dry!');
        }

        $issued = new DateTimeImmutable();
        $expiration = $issued->add(new DateInterval("PT" . \Base::instance()->get('SECURITY.exam_expiration') . "S"));
        $signer = new Sha256();
        $keychain = new Key("file://" . \Base::instance()->get('SECURITY.privatekey_path'));
        $token = (new Builder())->issuedBy(\Base::instance()->get('SECURITY.issuer'))
            ->issuedAt($issued)
            ->expiresAt($expiration)
            ->withClaim('uid', $this->uuid)
            ->withClaim('uid-as', $as)
            ->withClaim('role', $this->acl->name)
            ->withClaim('iplid', $iploginId)
            ->getToken($signer,  $keychain);
        return $token;
    }

    public static function getFromHTTPHeader()
    {
        if (!\Base::instance()->exists('HEADERS.Authorization')) {
            return false;
        }
        $auth_header = \Base::instance()->get('HEADERS.Authorization');
        $auth_header = \substr($auth_header, strlen("Bearer "));
        $token = (new Parser())->parse($auth_header);

        $data = new ValidationData(); // It will use the current time to validate (iat, nbf and exp)
        $data->setIssuer(\Base::instance()->get('SECURITY.issuer'));


        $signer = new Sha256();
        $keychain = new Keychain();

        if (!$token->validate($data) || !$token->verify(
            $signer,
            $keychain->getPublicKey("file://" . \Base::instance()->get('SECURITY.publickey_path'))
        )) {
            return false;
        }

        $user = new self();
        $user->fields(["password"], true);
        $user->load(['uuid LIKE ?', $token->getClaim('uid')]);
        if ($user->dry()) {
            return false;
        }
        return $user;
    }


    public function cast($obj = NULL, $rel_depths = 1, $save_cast = true)
    {
        $obj = parent::cast($obj, $rel_depths);
        if (!$save_cast) {
            return $obj;
        } else {
            unset($obj['password']);
            return $obj;
        }
    }

    public function getLoggerInstance($loggerName = "general"): Logger
    {
        $log = new Logger($loggerName);
        $log->pushHandler(new StreamHandler("app" . DIRECTORY_SEPARATOR . "logs" . DIRECTORY_SEPARATOR . "onlineuser" . DIRECTORY_SEPARATOR . "onlineuser-log-essential.log", Logger::NOTICE));
        $log->pushHandler(new StreamHandler("app" . DIRECTORY_SEPARATOR . "logs" . DIRECTORY_SEPARATOR . "onlineuser" . DIRECTORY_SEPARATOR . "onlineuser-log-verbose.log", Logger::INFO));

        return $log;
    }
}
