<?php

namespace Model\Ujian\Online;

use Model\ModelBase;
use Model\System\OnlineUser;

class AnswerSlotOnline extends ModelBase
{
    protected
        $fieldConf = array(

            'format' => [
                'type' => \DB\SQL\Schema::DT_TEXT,
                'nullable' => false,
                'index' => false,
                'unique' => false,
                '_copyable' => true
            ],
            'exam' => [
                'nullable' => false,
                '_copyable' => true,
                'belongs-to-one' => '\Model\Ujian\Exam',
            ],
            'submissions' => [
                'nullable' => true,
                '_copyable' => false,
                'has-many' => ['\Model\Ujian\Online\SubmissionOnline', 'answer_slot'],

            ],

            'deleted_on' => [
                'type' => \DB\SQL\Schema::DT_DATETIME,
                'nullable' => true,
                'index' => false,
                'unique' => false,
            ],
            'created_on' => [
                'type' => \DB\SQL\Schema::DT_DATETIME,
                'nullable' => true,
                'index' => false,
                'unique' => false,
            ],
            'updated_on' => [
                'type' => \DB\SQL\Schema::DT_DATETIME,
                'nullable' => true,
                'index' => false,
                'unique' => false,
            ],

        ),
        $db = 'DB',
        $table = 'ujian_online_answer_slot';


    public function set_deleted_on($date)
    {
        return date("Y-m-d H:i:s", $date);
    }

    public function set_created_on($date)
    {
        return date("Y-m-d H:i:s", $date);
    }

    public function set_updated_on($date)
    {
        return date("Y-m-d H:i:s", $date);
    }

    public function save()
    {
        if (!$this->created_on)
            $this->created_on = time();
        $this->updated_on = time();
        return parent::save();
    }

    public function simulateFormat($as_participant)
    {
        return \preg_replace_callback("/\%([a-zA-Z0-9\_]+)\%/mi", function ($match) use ($as_participant) {
            if ($as_participant->{$match[1]}) {
                return $as_participant->{$match[1]};
            } else {
                return "-&&-"; // means it's imposibble.
            }
        }, $this->format);
    }

    public function submit(string $filepath, OnlineUser $as_participant): SubmissionOnline
    {
        if ($as_participant) {
            //Get submission of participant
            $submission = new \Model\Ujian\Online\SubmissionOnline();

            $submission->has("submission_owner", ["uuid LIKE ?", $as_participant->uuid]);
            $submission->has("answer_slot", ["id = ?", $this->_id]);
            $submission->load();
            // echo "Participant ID: ".$as_participant->uuid;
            if ($submission->dry()) {
                $submission->copyfrom([
                    "submission_owner" => $as_participant,
                    "answer_slot" => $this->_id
                ]);
                $submission->save();
            }

            // copy filepath:
            $fullpath = $this->exam->getFullPath() . DIRECTORY_SEPARATOR . $submission->stored_filename;
            if (is_file(($fullpath))) { // remove existing file
                unlink($fullpath);
            }

            copy($filepath, $fullpath);

            return $submission;
        }
        return null;
    }

    public function getSubmissions(OnlineUser $as_participant)
    {
        if ($as_participant) {
            $submission = new SubmissionOnline();
            $submission->has("submission_owner", ["uuid LIKE ?", $as_participant->uuid]);
            $submission->has("answer_slot", ["id = ?", $this->_id]);
            $submission->load();
            return $submission;
        }
        return [];
    }

    public function touch($key = "updated_on", $timestamp = null)
    {
        parent::touch($key, $timestamp);
    }

    public function castFormatToParticipant($obj =  NULL, $rel_depths = 1, $save_cast = true, OnlineUser $as_participant = null)
    {
        $obj = parent::cast($obj, $rel_depths);
        if ($as_participant === null) {
            throw new \Exception("Participant is dry.");
        }
        $obj['format'] = $this->simulateFormat($as_participant);
        if (!$save_cast) {
            return $obj;
        } else {
            unset($obj['submissions']); //will prevent leaking information
            return $obj;
        }
    }
}
