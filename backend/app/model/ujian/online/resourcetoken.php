<?php

namespace Model\Ujian\Online;

use Model\ModelBase;

class ResourceToken extends ModelBase
{
    protected
        $fieldConf = array(
            "token" => [
                'type' => \DB\SQL\Schema::DT_TEXT,
                'nullable' => false,
                'index' => false,
                'unique' => false,
                '_copyable' => true
            ],
            "resource" => [
                'nullable' => false,
                '_copyable' => true,
                "has-many" => array('\Model\Ujian\Online\ExamResource', "token")
            ],
            'valid_from' => [
                'type' => \DB\SQL\Schema::DT_DATETIME,
                'nullable' => false,
                'index' => false,
                'unique' => false,
                '_copyable' => true
            ],
            'valid_until' => [
                'type' => \DB\SQL\Schema::DT_DATETIME,
                'nullable' => false,
                'index' => false,
                'unique' => false,
                '_copyable' => true
            ],

            'deleted_on' => [
                'type' => \DB\SQL\Schema::DT_DATETIME,
                'nullable' => true,
                'index' => false,
                'unique' => false,
            ],
            'created_on' => [
                'type' => \DB\SQL\Schema::DT_DATETIME,
                'nullable' => true,
                'index' => false,
                'unique' => false,
            ],
            'updated_on' => [
                'type' => \DB\SQL\Schema::DT_DATETIME,
                'nullable' => true,
                'index' => false,
                'unique' => false,
            ],

        ),
        $db = 'DB',
        $table = 'ujian_online_resource_token';


    public function set_deleted_on($date)
    {
        return date("Y-m-d H:i:s", $date);
    }

    public function set_created_on($date)
    {
        return date("Y-m-d H:i:s", $date);
    }

    public function set_updated_on($date)
    {
        return date("Y-m-d H:i:s", $date);
    }


    public function save()
    {
        if (!$this->created_on) {
            $this->created_on = time();
        }

        if (!$this->token) {
            $this->token = bin2hex(random_bytes(16));
        }

        if (!$this->valid_until) {
            if ($this->resource->exam) {
                $this->valid_until = $this->resource->exam->time_ended;
            } else {
                $this->valid_until = time() + 43200; // add 12 hour from creation
            }
        }

        $this->updated_on = time();
        return parent::save();
    }
}
