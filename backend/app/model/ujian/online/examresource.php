<?php

namespace Model\Ujian\Online;

use DateInterval;
use DateTime;
use Exception;
use \Model\Ujian\Exam;
use Model\ModelBase;
use Ramsey\Uuid\Uuid;

class ExamResource extends ModelBase
{
    protected
        $fieldConf = array(
            "exam" => [
                'belongs-to-one' => '\Model\Ujian\Exam',
                "nullable" => false,
            ],
            'upload_name' => [
                'type' => \DB\SQL\Schema::DT_TEXT,
                'nullable' => false,
                '_copyable' => true
            ],
            'stored_filename' => [
                'type' => \DB\SQL\Schema::DT_TEXT,
                'nullable' => false,
                'index' => false,
                'unique' => false,
                '_copyable' => true
            ],
            "token" =>
            [
                "nullable" => false,
                '_copyable' => true,
                "belongs-to-one" => '\Model\Ujian\Online\ResourceToken',
            ],

            'deleted_on' => [
                'type' => \DB\SQL\Schema::DT_DATETIME,
                'nullable' => true,
                'index' => false,
                'unique' => false,
            ],
            'created_on' => [
                'type' => \DB\SQL\Schema::DT_DATETIME,
                'nullable' => true,
                'index' => false,
                'unique' => false,
            ],
            'updated_on' => [
                'type' => \DB\SQL\Schema::DT_DATETIME,
                'nullable' => true,
                'index' => false,
                'unique' => false,
            ],
        ),
        $db = 'DB',
        $table = 'ujian_online_resource';

    const TOKEN_VALID_DURATION =  1800;

    public function set_deleted_on($date)
    {
        return date("Y-m-d H:i:s", $date);
    }

    public function set_created_on($date)
    {
        return date("Y-m-d H:i:s", $date);
    }

    public function set_updated_on($date)
    {
        return date("Y-m-d H:i:s", $date);
    }


    public function save()
    {
        if (!$this->created_on)
            $this->created_on = time();
        if (!$this->stored_filename)
            $this->stored_filename = bin2hex(random_bytes(16));
        $this->updated_on = time();
        return parent::save();
    }

    /**
     * Get the full path of the resurce based on exam_unicode and stored_filename of the resource
     */
    public function getFullPath()
    {
        return $this->exam->getFullPath() . DIRECTORY_SEPARATOR . "resource" . DIRECTORY_SEPARATOR . $this->stored_filename;
    }

    /**
     * Handling upload resource file
     */
    public function submit(string $filepath, Exam $exam)
    {
        if (!$exam || $exam->dry()) {
            //TODO: Throw error exam must exist / or exam id required
            throw new \Model\Error(
                "Exam is dry!",
                "Exam is dry!",
                "SomeErrCode007"
            );
        }

        //Create new resource model
        $resource = new self;

        //explode $filepath to get file name
        $fileNameArr = explode("uploads/", $filepath);
        if (sizeof($fileNameArr) && $fileNameArr[1]) {

            //store file name to upload_name
            $resource->upload_name = $fileNameArr[1];
            $examFullPath = $exam->getFullPath();
            $resourceFullPath = $examFullPath . DIRECTORY_SEPARATOR . "resource";

            if (!is_dir($resourceFullPath)) {
                mkdir($resourceFullPath, 0744);
            }

            //Generating new unique resource name for an exam
            $uuid = Uuid::uuid4();
            //Converting UUID format to GUID format
            $guid = bin2hex($uuid->getBytes());
            //path will be like /app/uploads/{exam_uniqcode}/resource/{guid}
            $resourceFullPath = $resourceFullPath . DIRECTORY_SEPARATOR . $guid;

            //Check if there is previous file, then remove it
            // if (is_file($resourceFullPath)) {
            //     unlink($resourceFullPath);
            // }

            //Copy from filepath to the new resource path
            try {
                copy($filepath, $resourceFullPath);
            } catch (Exception $e) {
                throw new \Model\Error("Something unexpected happened on moving file!", 500, $e);
            }


            // //Remove uploaded file from app/uploads directory
            // if (is_file($filepath)) {
            //     unlink($filepath);
            // }

            //Linking resource to exam
            $resource->exam = $exam->id;

            //storing guid as stored_filename
            $resource->stored_filename = $guid;

            //Check if exam already has resource
            //We can just use the same token for the resource of the same exam
            $otherResource = new self;
            $otherResource->load(["exam = ?", $exam->id]);
            if ($otherResource->dry()) {
                //No resource has been linked with the exam
                //create new token
                //Generating new unique uuid as token
                $uuid = Uuid::uuid4();
                //Converting UUID format to GUID format
                $guid = bin2hex($uuid->getBytes());
                $token = $guid;

                //Create new resource token model
                $resourceToken = new \Model\Ujian\Online\ResourceToken();
                //Assign the newly created guid as token
                $resourceToken->token = $token;

                try {
                    //Get exam start time
                    $startTime = DateTime::createFromFormat("Y-m-d H:i:s", $exam->time_start);
                    //Create interval 10 minutes before exam start
                    $downloadInterval  = new DateInterval('PT10M');
                    //Adding interval to start time
                    $validFromTime = $startTime->sub($downloadInterval);

                    //assign new time as valid_from time
                    $resourceToken->valid_from = $validFromTime->format("Y-m-d H:i:s");

                    //Create interval 30 minutes after exam start
                    $limitDownloadInterval = new DateInterval("PT" . ExamResource::TOKEN_VALID_DURATION . "S");
                    //Get exam start time
                    $endTime = DateTime::createFromFormat("Y-m-d H:i:s", $exam->time_start);
                    //Adding interval to end time
                    $validUntilTime = $endTime->add($limitDownloadInterval);
                    //assign new time as valid_from time
                    $resourceToken->valid_until = $validUntilTime->format("Y-m-d H:i:s");
                    //Save the model to database
                    $resourceToken->save();
                    //Assign newly created token as return value
                    $token = $resourceToken;
                } catch (Exception $e) {
                    throw new \Model\Error("Something unexpected happened!", 500, $e);
                }
            } else {
                $token = $otherResource->token;
            }
            $resource->token = $token->id;

            //save the newly created resource model data to db
            $resource->save();
        } else {
            //TODO: Throw error name cannot be empty
            throw new \Model\Error(
                "Filename cannot be empty",
                "Empty filename",
                "SomeErrCode008"
            );
        }
        return $resource;
    }

    /**
     * Updating timestamp on a resource file
     */
    public function touch($key = "updated_on", $timestamp = null)
    {
        parent::touch($key, $timestamp);
    }

    public function cast($obj = NULL, $rel_depths = 1, $save_cast = true)
    {
        $obj = parent::cast($obj, $rel_depths);
        if (!$save_cast) {
            return $obj;
        } else {
            unset($obj['stored_filename']);
            return $obj;
        }
    }
}
