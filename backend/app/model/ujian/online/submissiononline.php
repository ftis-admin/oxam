<?php

namespace Model\Ujian\Online;

use Model\ModelBase;
use Model\System\OnlineUser;
use Ramsey\Uuid\Uuid;

class SubmissionOnline extends ModelBase
{
    protected
        $fieldConf = array(
            'submission_owner' => [
                'nullable' => false,
                '_copyable' => true,
                'belongs-to-one' => '\Model\System\OnlineUser',
            ],
            'answer_slot' => [
                'nullable' => false,
                '_copyable' => true,
                'belongs-to-one' => '\Model\Ujian\Online\AnswerSlotOnline',
            ],
            'stored_filename' => [
                'type' => \DB\SQL\Schema::DT_TEXT,
                'nullable' => false,
                'index' => false,
                'unique' => false,
                '_copyable' => false
            ],

            'deleted_on' => [
                'type' => \DB\SQL\Schema::DT_DATETIME,
                'nullable' => true,
                'index' => false,
                'unique' => false,
            ],
            'created_on' => [
                'type' => \DB\SQL\Schema::DT_DATETIME,
                'nullable' => true,
                'index' => false,
                'unique' => false,
            ],
            'updated_on' => [
                'type' => \DB\SQL\Schema::DT_DATETIME,
                'nullable' => true,
                'index' => false,
                'unique' => false,
            ],
        ),
        $db = 'DB',
        $table = 'ujian_online_submission';

    public function set_deleted_on($date)
    {
        return date("Y-m-d H:i:s", $date);
    }

    public function set_created_on($date)
    {
        return date("Y-m-d H:i:s", $date);
    }

    public function set_updated_on($date)
    {
        return date("Y-m-d H:i:s", $date);
    }

    public function save()
    {
        if (!$this->created_on)
            $this->created_on = time();
        if (!$this->stored_filename) {
            $uuid = Uuid::uuid4();
            //Converting UUID format to GUID format
            $guid = bin2hex($uuid->getBytes());
            $this->stored_filename = $guid;
        }
        $this->updated_on = time();
        return parent::save();
    }

    public function getFullPath(): string
    {
        return $this->answer_slot->exam->getFullPath() . DIRECTORY_SEPARATOR . $this->stored_filename;
    }

    public function touch($key = "updated_on", $timestamp = null)
    {
        parent::touch($key, $timestamp);
    }
}
