<?php

namespace Controller\Api\System;

use Controller\Api\Base;

class OnlineUser extends Base
{
    public function get_me()
    {
        /**
         * Check if Authorization header is exist.
         * If Authorization header exist take the token
         * Get Lecturer model using uuid claim in token
         */
        parent::precheck_must_login_ldap();
        $lecturer = \Model\System\OnlineUser::getFromHTTPHeader();

        $response = ["profile" => $lecturer->cast(null, 1)];
        unset($response["profile"]["participating_exams"]);
        unset($response["profile"]["exam_submission"]);
        unset($response["profile"]["exams"]);
        unset($response["profile"]["acl"]["_id"]);

        return \View\Api::success($response);
    }

    public function post_me($f3)
    {
        // parent::precheck_must_login_ldap();
        // $lecturer = 
        // $response = ["profile" => $user->cast()];

        // return \View\Api::success($response);
    }
}
