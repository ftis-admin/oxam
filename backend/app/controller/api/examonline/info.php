<?php

namespace Controller\Api\ExamOnline;

use Controller\Api\ManageOnline\AnswerSlot;
use Controller\CRUDBase;
use Model\Error;
use Model\System\AclItem;
use Model\System\OnlineUser;
use Model\Ujian\Exam;
use Model\Ujian\Online\AnswerSlotOnline;
use Model\Ujian\Online\NotificationOnline;
use Model\Ujian\Online\ResourceToken;

class Info extends CRUDBase
{

    protected $permissionPrefix = "manage-ujian-exam-online";
    protected $model = "\\Model\\Ujian\\Exam";

    public function get_index($f3)
    {
        $this->permission_check_ldap($this->permissionPrefix, AclItem::NONE);

        //Get currently logged in participant
        $participant = OnlineUser::getFromHTTPHeader();

        if ($participant) {
            //UUID claim in JWT token valid and user exists in database

            //Create new Exam model for filter
            $examModel = new Exam();

            //Removing unecessary data for the output
            $examModel->fields([
                // "online_participants",
                "lecturers",
                "exam_report",
                "participants",
                "answer_slot",
            ], true);

            //Find all exam where the user logged in participating
            $examModel->has("online_participants", ["uuid LIKE ?", $participant->uuid]);

            //Find all exam that has time_start - 10 minutes equals or bigger than current time,
            //Find all exam that has time_stop bigger than current time
            $upcomingTimeMinutes = 10;
            $exams = $examModel->find([
                "((time_start >= ? and time_ended = ?) or time_ended >= ?) and deleted_on = ? and online = ?",
                date('Y-m-d H:i:s', strtotime(("-$upcomingTimeMinutes minute"))),
                null,
                date('Y-m-d H:i:s'),
                null,
                1
            ], ['limit' => 1, 'order' => 'time_start ASC']);

            if (!$exams) {
                $exams = [];
            } else {
                $exams = $exams->castAll();

                foreach ($exams as $key => $value) {
                    //Removing unnecessary and confidential fields in exam
                    unset($value["online_participants"]);
                    unset($value["notification"]);

                    if ($value["resource"]) {
                        $tempResource = [];
                        foreach ($value["resource"] as $resource) {
                            //Get token data
                            $token = new ResourceToken();
                            $token->fields(["token"]);
                            $token->load(["_id = ?", $resource["token"]]);

                            if ($token->loaded()) {
                                $resource["token"] = $token->cast();
                            }
                            $tempResource[] = $resource;
                        }
                        $value["resource"] = $tempResource;
                    }

                    //Formatting proper answer slot
                    if ($value["online_answer_slot"]) {
                        $value["online_answer_slot"] = array_map(function ($answer_slot) use ($participant) {

                            $ansSlotObj = new AnswerSlotOnline();
                            // echo $answer_slot["_id"];
                            $ansSlotObj->load(["_id = ?", $answer_slot["_id"]]);

                            if ($ansSlotObj->loaded() === 0) {
                                return null;
                            }

                            if ($ansSlotObj->deleted_on !== null) {
                                return null;
                            }

                            return $ansSlotObj->castFormatToParticipant(null, 0, true, $participant);

                            // $answer_slot["format"] = 
                        },  (array) $value["online_answer_slot"]);
                    }

                    $value["online_answer_slot"] = array_map(function ($answer_slot) use ($participant) {

                        $ansSlotObj = new AnswerSlotOnline();
                        // echo $answer_slot["_id"];
                        $ansSlotObj->load(["_id = ?", $answer_slot["_id"]]);

                        if ($ansSlotObj->loaded() === 0) {
                            return null;
                        }

                        if ($ansSlotObj->deleted_on !== null) {
                            return null;
                        }

                        return $ansSlotObj->castFormatToParticipant(null, 0, true, $participant);

                        // $answer_slot["format"] = 
                    },  (array) $value["online_answer_slot"]);


                    $exams[$key] = $value;
                }
                //Return as object instead of Array
                $exams = $exams[0];
                //Load notifications belong to logged in user for this exam
                // $participant->has("notifications.exam", ["id = ?", $exams["_id"]]);
                // $result = $participant->load(["uuid LIKE ?", $participant->uuid]);
                // echo $result;
                //Removing unnecessary field from participant
                // $participant->fields(["notifications.exam"], true);
                if ($participant->notifications) {
                    $notifications = array_map(function ($notif) use ($exams) {
                        // echo ($notif->exam->_id == $exams["_id"] ? "True" : "False");
                        // echo "Exam ID: " . $exams["_id"];
                        // echo "Notif Exam ID: " . $notif["exam"]["_id"];

                        if ($notif["exam"]["_id"] == $exams["_id"]) {
                            unset($notif["exam"]);
                            return $notif;
                        }
                        return [];
                    }, $participant->notifications->castAll());
                } else {
                    $notifications = [];
                }

                $participantObj = $participant->cast();
                $participantObj["participating_exams"] = $exams;
                $participantObj["notifications"] = $notifications;

                //Removing unecessary fields
                unset($participantObj["exam_submission"]);
                unset($participantObj["exams"]);
                unset($participantObj["acl"]["_id"]);

                $participant =  $participantObj;
            }

            return \View\Api::success($participant);
        } else {
            //Throw err
        }
    }
}
