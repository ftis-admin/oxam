<?php

namespace Controller\Api\ExamOnline;

use Controller\CRUDBase;
use DateTime;
use DateTimeZone;
use Model\System\AclItem;
use  Model\System\OnlineUser;
use Model\Error as ModelError;
use Model\Ujian\Online\ResourceToken;
use Respect\Validation\Validator as v;

use Respect\Validation\Exceptions\NestedValidationException;
use InvalidArgumentException;

class ResourceOnline extends CRUDBase
{
    protected $permissionPrefix = "manage-ujian-resource-online";
    protected $model = "\\Model\\Ujian\\Online\\ExamResource";

    public function post_item_download($f3)
    {
        $this->permission_check_ldap($this->permissionPrefix, AclItem::NONE);

        //Get resource by id in the request url
        $resource = parent::getMentionedItem($f3);

        //Get currently logged in user using the Authorization header
        $participant = OnlineUser::getFromHTTPHeader();

        if ($participant) {
            $owner = new OnlineUser();
            //Filter OnlineUser that has exam corresponds to exam assigned to resource
            $owner->has('participating_exams', ["id = ?", $resource->exam->id]);

            //Filter all owner by uuid that match the currently logged in user
            $owners = $owner->find(["uuid LIKE ?", $participant->uuid]);

            if ($owners && sizeof($owners) == 1) {
                //Validating token
                try {
                    $validator = v::key("token", v::notOptional()->mustExists("\\Model\\Ujian\\Online\\ResourceToken", 'token'));
                    $validator->assert($f3->POST);

                    $resourceToken = new ResourceToken();

                    $resourceToken->load([
                        "token LIKE ? and deleted_on = ?",
                        $f3->POST["token"], null
                    ]);


                    if ($resourceToken->dry()) {
                        //If no Token exist, throw error token invalid
                        throw new InvalidArgumentException("Token is invalid.");
                    } else {

                        //Just keeping this thing here, in case of emergency
                        // $tz = $f3->get('TZ');
                        // $timestamp = time();
                        // $dt = new DateTime("now", new DateTimeZone($tz));
                        // $dt->setTimestamp($timestamp);
                        // $currentTime = $dt->format('Y-m-d H:i:s');

                        //Get current time with timezone
                        $currentTime = date('Y-m-d H:i:s');

                        //Start time minus 10 minutes
                        //Allow resource download 10 minute before exam start
                        $upcomingTimeMinutes = 10;
                        $preDownloadTime = date('Y-m-d H:i:s', strtotime(("+$upcomingTimeMinutes minute")));

                        if ($resourceToken->valid_from <= $preDownloadTime && $resourceToken->valid_until >= $currentTime) {
                            //Token exist in database and still valid
                            //Check if specified token matches the requested resource
                            if ($resourceToken->id === $resource->token->id) {
                                // print_r($resource->cast());
                                //Specified token matches the requested resource
                                //Get full path of the resource
                                $resourceFullPath = $resource->getFullPath();
                                //Get upload name of the resource
                                $resourceUploadName = $resource->upload_name;

                                //Sending file to client side
                                header("X-Filename: $resourceUploadName");
                                \Web::instance()->send(
                                    $resourceFullPath,
                                    null,
                                    0,
                                    true,
                                    $resourceUploadName
                                );
                            } else {
                                //Throw error, specified token doesn't match the requested resource
                                throw new InvalidArgumentException("Invalid token specified for requested resource.", 400);
                            }
                        } else {
                            //Token is no longer valid
                            throw new InvalidArgumentException("Either token is no longer valid or not yet valid. Ask Administrator for more information.", 401);
                        }
                    }
                } catch (NestedValidationException $e) {
                    throw \Helper\Ruler::transformToError($e);
                } catch (InvalidArgumentException $e) {
                    throw new ModelError("Invalid Input", $e->getMessage(), "X400", "Exception", $e->getCode());
                }
            } else {
                //TODO: Throw error don't have privilege to access the resource
                throw new ModelError("Resource Download Failed", "You don't have privilege to access the resource", "403", "Authorization Violation");
            }
        } else {
            throw new ModelError("You need to login before you can do that", "Bad Auth!", "403", "Authorization Violation");
        }
    }
}
