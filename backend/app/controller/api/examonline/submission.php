<?php

namespace Controller\Api\ExamOnline;

use Model\System\OnlineUser;
use Model\Error;
use DateTime;
use DateTimeZone;

class Submission extends \Prefab
{
    public function get_submit($f3)
    {
        //Get current user from JWT Token
        $participant = OnlineUser::getFromHTTPHeader();

        if (!$participant) {
            //Throw error, user need to login first
            throw new Error(
                "Bad Auth",
                "You need to login to do that.",
                "403",
                "No Authless Permitted",
                403
            );
        } else {
            //Check if answerslot id for submission is exist in request parameter
            if (!$f3->exists("GET.answer_slot")) {
                throw new \Model\Error(
                    "Incomplete Request",
                    "We need the `answer_slot` ID to do that",
                    "ES03"
                );
            }

            //Querying answerslot by id
            $answerSlot = new \Model\Ujian\Online\AnswerSlotOnline();
            $answerSlot->load(["id = ?", $f3->get("GET.answer_slot")]);

            if ($answerSlot->dry()) {
                throw new \Model\Error(
                    "Bad Request",
                    "The given `answer_slot` is invalid.",
                    "ES04"
                );
            }

            //Query exam, check if exam has online_participant with UUID equals
            //to the one the curentlly loged in user have and id equals to exam id in answerslot
            //Also check if exam is in progress and has not ended
            //in progress: time_ended not null, current_time <= time_ended, current_time >= time_start
            //Also only query exam that has not been marked deleted

            $tz = $f3->get('TZ');
            $timestamp = time();
            $dt = new DateTime("now", new DateTimeZone($tz));
            $dt->setTimestamp($timestamp);
            $date = $dt->format('Y-m-d H:i:s');

            $exam =  new \Model\Ujian\Exam();
            $exam->has("online_participants", ["uuid LIKE ?", $participant->uuid]);
            $exam->load([
                "_id = ? AND time_start <= ? AND (time_ended != ? AND time_ended >= ? ) and deleted_on = ?",
                $answerSlot->exam->id,
                $date,
                NULL,
                $date,
                NULL
            ]);

            if ($exam->dry()) {
                throw new \Model\Error(
                    "Unexpected Upload",
                    "Answer submission has been closed. Please check your exam time.",
                    "ES07"
                );
            }


            //Get submission of participant
            $submission = new \Model\Ujian\Online\SubmissionOnline();

            $submission->has("submission_owner", ["uuid LIKE ?", $participant->uuid]);
            $submission->has("answer_slot", ["id = ?", $answerSlot->id]);
            $submission->load();

            if ($submission->dry()) {
                throw new Error("No submission found", "Submission not found", "HTTP404", "no reason", 404);
            }

            if ($f3->exists("GET.force_download")) {
                \Web::instance()->send(
                    $submission->getFullPath(),
                    null,
                    0,
                    true,
                    $submission->answer_slot->simulateFormat($participant)
                );
            } else {
                $submission = $submission->cast();
                unset($submission["stored_filename"]);
                return \View\Api::success($submission);
            }
        }
    }

    /**
     * Handle student exam submissions
     */
    public function post_submit($f3)
    {
        //Get current user from JWT Token
        $participant = OnlineUser::getFromHTTPHeader();

        if (!$participant) {
            //Throw error, user need to login first
            throw new Error(
                "Bad Auth",
                "You need to login to do that.",
                "403",
                "No Authless Permitted",
                403
            );
        } else {
            //Check if answerslot id for submission is exist in request parameter
            if (!$f3->exists("POST.answer_slot")) {
                throw new \Model\Error(
                    "Incomplete Request",
                    "We need the `answer_slot` ID to do that",
                    "ES03"
                );
            }

            //Querying answerslot by id
            $answerSlot = new \Model\Ujian\Online\AnswerSlotOnline();
            $answerSlot->load(["id = ?", $f3->get("POST.answer_slot")]);

            if ($answerSlot->dry()) {
                throw new \Model\Error(
                    "Bad Request",
                    "The given `answer_slot` is invalid.",
                    "ES04"
                );
            }

            //Query exam, check if exam has online_participant with UUID equals
            //to the one that curentlly logged  has and exam id equals to exam id in answerslot
            //Also check if exam is in progress and has not ended
            //in progress: time_ended not null, current_time <= time_ended, current_time >= time_start
            //Also only query exam that has not been marked deleted

            $tz = $f3->get('TZ');
            $timestamp = time();
            $dt = new DateTime("now", new DateTimeZone($tz));
            $dt->setTimestamp($timestamp);
            $date = $dt->format('Y-m-d H:i:s');

            $exam =  new \Model\Ujian\Exam();
            $exam->has("online_participants", ["uuid LIKE ?", $participant->uuid]);
            $exam->load([
                "_id = ? AND time_start <= ? AND (time_ended != ? AND time_ended >= ? ) and deleted_on = ?",
                $answerSlot->exam->id,
                $date,
                NULL,
                $date,
                NULL
            ]);

            //If no exam found, throw error, exam not exist or exam ended
            if ($exam->dry()) {
                throw new \Model\Error(
                    "Unexpected Upload",
                    "Answer submission has been closed. Please check your exam time.",
                    "ES07"
                );
            }

            //Receive submit file from client
            $web = \Web::instance();
            $files = $web->receive(
                function ($file, $formFieldName) use ($answerSlot, $participant) {
                    return (basename($file['name']) == $answerSlot->simulateFormat($participant));
                },
                true,
                false
            );

            //Make sure to only accecpt one file per request
            if (count($files) > 1) {
                throw new \Model\Error(
                    "Bad news",
                    "Given answer is not singular. Send one-by-one please.",
                    "ES05"
                );
            }

            if (!$files[array_keys($files)[0]]) {
                throw new \Model\Error(
                    "Invalid Filename",
                    "Given filename is not respecting slot format. Got: " . array_keys($files)[0],
                    "ES06"
                );
            }

            $file = array_keys($files)[0];
            $submission = $answerSlot->submit($file, $participant);
            $submission->touch();
            $submission->save();

            if ($submission->loaded() == 0) {
                $submission = [];
            } else {
                $submission = $submission->cast();
                unset($submission["stored_filename"]);
                unset($submission["submission_owner"]);
            }

            return \View\Api::success($submission);
        }
    }
}
