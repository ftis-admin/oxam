<?php

namespace Controller\Api\Manage;

use Model\System\OnlineUser;
use  Model\System\AclItem;
use Model\Error;
use Controller\CRUDBase;
use Exception;

class Lecture extends CRUDBase
{
    protected $permissionPrefix = "manage-ujian-lecture";
    protected $model = "\\Model\\Ujian\\Lecture";

    /**
     * Checks the permission of current user, if no user logged in,
     * we'll check from public permission.
     * 
     * If user (instance of \Model\Ujian\OnlineUser) logged in,
     * then compare the acl owned by user with acl in permission prefix
     *
     * @param string $what ACL Name
     * @param string $mode CRUD mode selection
     * @return void
     */
    protected function permission_check($what, $mode)
    {
        $isLdap = true; 
        $isBasic = true;
        $errorMessage = [];

        try {
            $user = \Model\System\User::getFromHTTPHeader();
            if (!$user && ($this->publicPermission & $mode) == 0) {
                $isBasic = false;
            }
            return \Helper\Ijin::mustHave($what, $mode);
        } catch (Error $e) {
            $isBasic = false;
            $errorMessage = $e->serve_exception();
        }
        try {
            $lecturer = OnlineUser::getFromHTTPHeader();
            if (!$lecturer && ($this->publicPermission & $mode) == 0) {
                $isLdap = false;
            }
            return \Helper\Ijin::mustHave_Online($what, $mode);
        } catch (Error $e) {
            $isLdap = false;
            $errorMessage = $e->serve_exception();
        }

        if (!$isLdap && !$isBasic) {
            if (!count($errorMessage)) {
                throw new Error("Bad Auth", "You don't have permission to do that.", "403", "Bad Permission", 403);
            } else {
                throw new Error(
                    $errorMessage["title"],
                    $errorMessage["description"],
                    $errorMessage["error_code"],
                    $errorMessage["reason"],
                    403
                );
            }
        }
    }


    /**
     * List all object in a model.
     * May support search and paging. But not sure.
     *
     * @param object $f3 FatFree Object
     * @return void
     */
    public function get_index($f3)
    {
        // permission checkpoint
        $this->permission_check($this->permissionPrefix, AclItem::READ);

        $model = new $this->model;
        $this->globalFilterTrigger($model);
        if (in_array("deleted_on", $model->fields())) {
            $models = $model->find(["deleted_on = ?", null]);
        } else {
            $models = $model->find();
        }
        if ($models === false) {
            $models = [];
        } else {
            $models = $models->castAll();
        }
        return \View\Api::success($models);
    }
}
