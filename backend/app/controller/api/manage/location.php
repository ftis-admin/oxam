<?php

namespace Controller\Api\Manage;

use Controller\Api\Manage\Computer as ManageComputer;
use Controller\CRUDBase;
use Model\System\AclItem;
use Model\Ujian\Computer;
use Respect\Validation\Validator as v;
use Exception;
use InvalidArgumentException;
use \Respect\Validation\Exceptions\NestedValidationException;

class Location extends CRUDBase
{
    protected $permissionPrefix = "manage-ujian-location";
    protected $model = "\\Model\\Ujian\\Location";

    public function get_item_ping($f3)
    {
        // permission checkpoint
        $this->permission_check($this->permissionPrefix, AclItem::READ);

        //Get location by location id in request url  
        $location = $this->getMentionedItem($f3);
        $result = [];
        try {
            $validator =
                //validate key ip is a valid ip address and in range or location subnet
                v::key("ip", v::notOptional()->ip($location->subnet_start . "/24", FILTER_FLAG_IPV4)
                    //Validate ip is associated with at least one computer
                    ->mustExists("\\Model\\Ujian\\Computer", "ip", "delete_on"));

            //Validating
            $validator->assert($f3->GET);

            //Get ip address from the request
            $ip = $f3->GET["ip"];

            //Output holder
            $output = [];
            //Exec result code
            $result_code = -1;

            //Ping command
            //Ping for IPv4 (-4) with count (-c) of 2 for $ip
            $command = "ping -4 -c 2 " . escapeshellcmd($ip);

            //Executing the ping command
            exec($command, $output, $result_code);

            //Implode the output array to a string for regex operation
            $outputString = implode(" ", $output);

            //Regex to check packet success rate
            $re = '/([0-9]+)% packet loss/s';

            //Regexing
            preg_match($re, $outputString, $matches, PREG_OFFSET_CAPTURE, 0);

            return \View\Api::success(["ip" => $ip, "loss_rate" => intval($matches[1][0]), "loss" => $matches[0][0]]);
        } catch (NestedValidationException $e) {
            throw \Helper\Ruler::transformToError($e);
        }

        return \View\Api::success($result);
    }

    public function put_item_computer($f3)
    {
        // permission checkpoint
        $this->permission_check($this->permissionPrefix, AclItem::UPDATE);

        $location = $this->getMentionedItem($f3);

        try {
            $validator = v::key("computers", v::each(
                //validate key ip is a valid ip address and in range or location subnet
                v::key("ip", v::notOptional()->ip($location->subnet_start . "/24", FILTER_FLAG_IPV4))
                    //Validate pos_x is a number
                    ->key("pos_x", v::notOptional())
                    //Validate pos_y is a number
                    ->key("pos_y", v::notOptional())
                    //Validate priority is a number
                    ->key("priority", v::notOptional())
                    //Validate state is a number
                    ->key("state", v::notOptional())
            ));
            //Validate body request
            $validator->validate($f3->POST);

            //Get computers from body request
            $computers = $f3->POST["computers"];
            $idsOfComp = [];

            foreach ($computers as $computer) {
                $currComp = new Computer();
                $currComp->load(["ip LIKE  ?", $computer["ip"]]);

                if ($currComp->dry()) {
                    //Set current computer ip
                    $currComp->ip = $computer["ip"];
                }

                //Updating computer data;
                // $currComp->location = $location->_id;
                // $currComp->name = $computer["name"];
                // $currComp->reverse_dns = $computer["reverse_dns"];
                // $currComp->pos_x = $computer["pos_x"];
                // $currComp->pos_y = $computer["pos_y"];
                // $currComp->priority = $computer["priority"];
                // $currComp->state = $computer["state"];

                $currComp->copyfromwithfilter($computer);
                $currComp->location = $location->_id;
                $currComp->save();

                $currComp->load(["ip LIKE ?", $computer["ip"]]);

                array_push($idsOfComp, $currComp->_id);
            }

            $toBeDeletedComps = new Computer();
            // $loadedComp = [];

            $toBeDeletedComps->has("location", ["id = ?", $location->_id]);
            $toBeDeletedComps->load(["id NOT IN ?", $idsOfComp]);
            if ($toBeDeletedComps->loaded()) {
                $loaded = $toBeDeletedComps->loaded();
                for ($i = 0; $i < $loaded; $i++) {
                    $toBeDeletedComps->erase();
                    $toBeDeletedComps->next();
                }
            }

            return \View\Api::success($location->cast());
        } catch (NestedValidationException $e) {
            throw \Helper\Ruler::transformToError($e);
        }
    }
}
