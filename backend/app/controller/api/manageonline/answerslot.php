<?php

namespace Controller\Api\ManageOnline;

use Controller\CRUDBase;
use Model\System\AclItem;
use Model\System\OnlineUser;

class AnswerSlot extends CRUDBase
{
    protected $permissionPrefix = "manage-ujian-answerslot-online";
    protected $model = "\\Model\\Ujian\\Online\\AnswerSlotOnline";

    // public function  get_item($f3)
    // {
    //     $this->permission_check_ldap($this->permissionPrefix, AclItem::READ);

    //     $activeUser = OnlineUser::getFromHTTPHeader();
    //     $exam = $this->getMentionedItem($f3);

    //     if ($activeUser) {
    //         if ($activeUser->acl->name == "lecturer") {
    //             $answerSlots = $exam->online_answer_slot;
    //             if (!$answerSlots) {
    //                 $answerSlots = [];
    //             } else {
    //                 $answerSlots = $answerSlots->castAll();
    //             }
    //             return \View\Api::success($answerSlots);
    //         } else if ($activeUser->acl->name == "student") {
    //         }
    //     }
    // }

    public function put_item($f3)
    {
        parent::put_item($f3);
    }
}
