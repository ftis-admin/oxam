<?php

namespace Controller\Api\ManageOnline;

use Model\System\AclItem;
use Controller\CRUDBase;
use Model\System\OnlineUser;
use Respect\Validation\Exceptions\NestedValidationException;
use InvalidArgumentException;
use Model\Error as ModelError;
use Model\Ujian\Online\ResourceToken;
use Monolog\Logger;
use Respect\Validation\Validator as v;

class ResourceOnline extends CRUDBase
{
    protected $permissionPrefix = "manage-ujian-resource-online";
    protected $model = "\\Model\\Ujian\\Online\\ExamResource";


    public function post_item_download($f3)
    {
        //Permission check for logged in local or ldap
        $this->permission_check_local_ldap($this->permissionPrefix, AclItem::READ);

        //Get resource by id in the request url
        $resource = parent::getMentionedItem($f3);

        //Get currently logged in user using the Authorization header
        $onlineUser = \Model\System\OnlineUser::getFromHTTPHeader();
        // $admin = \Model\System\User::getFromHTTPHeader();

        if ($onlineUser && $onlineUser->acl->name == "lecturer") {

            $owner = new OnlineUser();
            //Filter OnlineUser that has exam corresponds to exam assigned to resource
            $owner->has('exams', ["id = ?", $resource->exam->id]);
            //Filter all owner by uuid that match the currently logged in user
            $owners = $owner->find(["uuid LIKE ?", $onlineUser->uuid]);

            //If the current user is not in list of the owners, then throw error
            if (sizeof($owners) == 1) {
                //Get full path of the resource
                $resourceFullPath = $resource->getFullPath();
                //Get upload name of the resource
                $resourceUploadName = $resource->upload_name;
                // echo $resourceFullPath;

                // Sending file to client side
                header("x-filename: $resourceUploadName");
                \Web::instance()->send(
                    $resourceFullPath,
                    null,
                    0,
                    true,
                    $resourceUploadName
                );
            } else {
                //TODO: Throw error don't haveprivilege to access the resource
            }
        } else if ($onlineUser && $onlineUser->acl->name == "student") {
            $owner = new OnlineUser();
            //Filter OnlineUser that has exam corresponds to exam assigned to resource
            $owner->has('participating_exams', ["id = ?", $resource->exam->id]);
            //Filter all owner by uuid that match the currently logged in user
            $owners = $owner->find(["uuid LIKE ?", $onlineUser->uuid]);

            if ($owners && sizeof($owners) == 1) {
                //Validating token
                try {
                    $validator = v::key("token", v::notOptional()->mustExists("\\Model\\Ujian\\Online\\ResourceToken", 'token'));
                    $validator->assert($f3->POST);

                    $resourceToken = new ResourceToken();

                    $resourceToken->load([
                        "token LIKE ?",
                        $f3->POST["token"]
                    ]);


                    if ($resourceToken->dry()) {
                        //If no Token exist, throw error token invalid
                        throw new InvalidArgumentException("Token is invalid.");
                    } else {
                        $date = date('Y-m-d H:i:s', time());
                        if ($resourceToken->valid_from < $date && $resourceToken->valid_until > $date) {
                            //Token exist in database and still valid
                            //Check if specified token matches the requested resource
                            if ($resourceToken->id === $resource->token->id) {
                                //Specified token matches the requested resource
                                //Get full path of the resource
                                $resourceFullPath = $resource->getFullPath();

                                //Get upload name of the resource
                                $resourceUploadName = $resource->upload_name;

                                //Sending file to client side
                                $size = \Web::instance()->send(
                                    $resourceFullPath,
                                    null,
                                    0,
                                    true,
                                    $resourceUploadName
                                );
                                
                            } else {
                                //Throw error, specified token doesn't match the requested resource
                                throw new InvalidArgumentException("Invalid token specified for requested resource.");
                            }
                        } else {
                            //Token is no longer valid
                            throw new InvalidArgumentException("Token is no longer valid.");
                        }
                    }
                } catch (NestedValidationException $e) {
                    throw \Helper\Ruler::transformToError($e);
                } catch (InvalidArgumentException $e) {
                    throw new ModelError("Invalid Input", $e->getMessage(), "X400", "Exception", 400);
                }
            } else {
                //TODO: Throw error don't have privilege to access the resource
                throw new ModelError("Resource Download Failed", "You don't have privilege to access the resource", "403", "Authorization Violation");
            }
        } else {
            return \View\Api::success("Downloading as nobody");        }
    }
}
