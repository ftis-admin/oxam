<?php

namespace Controller\Api\ManageOnline;

use Controller\CRUDBase;
use  Model\System\AclItem;
use Model\Error;

class Lecturer extends CRUDBase
{
    protected $permissionPrefix = "manage-system-user-online";
    protected $model = "\\Model\\System\\OnlineUser";


    public function get_index($f3)
    {
        //Check if user is loggged in
        $this->permission_check_local_ldap($this->permissionPrefix, AclItem::READ);
        $model = new $this->model;

        //Loading acl for lecturer, filter for user with ACL lecturer
        $aclModel =  new \Model\System\Acl();
        $aclModel->load(["name LIKE ?", "lecturer"]);
        // $models = false;

        //Should never happen, just in case handle with error
        if ($aclModel->dry()) {
            //handle exception
        } else {
            //Discard password from output
            $model->fields(["uuid", "display_name", "username"]);
            // var_dump($aclModel->cast());
            $models = $model->find(["acl = ?", $aclModel->id]);
        }

        // var_dump($models);

        if ($models === false) {
            $models = [];
        } else {
            $models = $models->castAll();
        }

        return \View\Api::success($models);
    }
}
