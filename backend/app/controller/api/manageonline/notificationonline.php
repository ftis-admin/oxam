<?php

namespace Controller\Api\ManageOnline;

use Controller\CRUDBase;
use InvalidArgumentException;
use Model\Error;
use Model\System\AclItem;
use Model\System\OnlineUser;
use Model\Ujian\Online\NotificationOnline as OnlineNotificationOnline;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator as v;

class NotificationOnline extends CRUDBase
{
    protected $permissionPrefix = "manage-ujian-notification-online";
    protected $model = "\\Model\\Ujian\\Online\\NotificationOnline";

    public function post_index($f3)
    {
        echo "Here";
    }

    public function put_item($f3)
    {
        // permission checkpoint
        $this->permission_check_local_ldap($this->permissionPrefix, AclItem::UPDATE);
        try {

            //Assert id is exist in request parameter
            $idValidator = v::key("id", v::notOptional()->mustExists($this->model, "id", "deleted_on"));
            $idValidator->assert($f3->PARAMS);

            //Assert participants list consist of valid UUID and not optional
            $validator = v::key("participants", v::notOptional()->each(v::mustExists("\\Model\\System\\OnlineUser", "uuid", "deleted_on")));
            $validator->assert($f3->POST);

            $updatedNotification = new OnlineNotificationOnline();
            $updatedNotification->load(["id = ?", $f3->PARAMS["id"]]);

            if ($updatedNotification->dry()) {
                throw new Error("Object not found", "Object cannot be found", "HTTP404", "Global Validation", 404);
            }
            //Copy updated data from request body
            $updatedNotification->copyfromwithfilter(array_diff_key($f3->POST, array_reverse(['id', '_id'])), true);

            $participantsUUID = $f3->POST["participants"];
            $participantsId = array_map(function ($uuid) {
                $currPar = new OnlineUser();
                $currPar->load(["uuid LIKE ?", $uuid]);

                if ($currPar->loaded()) {
                    return $currPar->_id;
                } else {
                    return;
                }
            }, $participantsUUID);

            $updatedNotification->participants = $participantsId;

            $updatedNotification->save();

            return \View\Api::success($updatedNotification->cast());
        } catch (NestedValidationException $e) {
            throw \Helper\Ruler::transformToError($e);
        } catch (InvalidArgumentException $e) {
            throw new Error("Invalid Input", $e->getMessage(), "X400", "Exception", 400);
        }
    }


    public function delete_item($f3, $directDelete = true)
    {
        parent::delete_item($f3, $directDelete);
    }

    public function post_mass_gen($f3)
    {
        //Permission check_point
        $this->permission_check_local_ldap($this->permissionPrefix, AclItem::CREATE);
        try {
            //List must consist of participant's uuid (this uuid will be crossedchecked with user in database), 
            //username of each participant, 
            //password of each participant, 
            //url of the service the username and password will be used for
            //and also exam id where the notification will be shown
            $validator = v::key("lists", v::each(
                v::key("participant", v::notOptional()->mustExists("\\Model\\System\\OnlineUser", "uuid", "deleted_on"))
                    ->key("username", v::notOptional())
                    ->key("password", v::notOptional())
            ))
                ->key("url", v::notOptional())
                ->key("exam", v::notOptional()->mustExists("\\Model\\Ujian\\Exam", "id", "deleted_on"));
            $validator->assert($f3->POST);


            $service = $f3->POST['url'];
            $examID = $f3->POST['exam'];
            $lists = $f3->POST['lists'];

            foreach ($lists as $val) {
                $participant = new OnlineUser();
                $participant->load(["uuid LIKE ?", $val["participant"]]);

                $notification = new $this->model;
                $notification->copyfrom([
                    "title" => "Akun untuk " . $service,
                    "type" => "credential",
                    "description" => \Template::instance()->render("notifications/password.html", 'text/html', [
                        "notif" => [
                            "url" => $service,
                            "username" => $val['username'],
                            "password" => $val['password'],
                        ]
                    ]),
                    "participants" => $participant,
                    "extras" => [
                        "service" => $service,
                        "username" => $val['username'],
                        "password" => $val['password'],
                    ]
                ]);
                $notification->exam = $examID;
                $notification->save();
            }

            return \View\Api::success([]);
        } catch (NestedValidationException $e) {
            throw \Helper\Ruler::transformToError($e);
        } catch (InvalidArgumentException $e) {
            throw new Error("Invalid Input", $e->getMessage(), "X400", "Exception", 400);
        }
    }
}
