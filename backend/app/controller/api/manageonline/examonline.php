<?php

namespace Controller\Api\ManageOnline;

use Controller\Api\ManageOnline\ResourceOnline;
use Model\System\OnlineUser;
use  Model\System\Acl;
use  Model\System\AclItem;
use Model\Error;
use Controller\CRUDBase;
use Exception;
use InvalidArgumentException;
use \Respect\Validation\Exceptions\NestedValidationException;
use Exception\NotParseable;
use Model\Ujian\Online\ExamResource;
use Model\Ujian\Online\NotificationOnline;
use Model\Ujian\Online\ResourceToken;
use Ramsey\Uuid\Uuid;
use Respect\Validation\Validator as v;
use DateInterval;
use DateTime;
use Model\System\User;
use Helper\AnswerZipper;

class ExamOnline extends CRUDBase
{
    protected $permissionPrefix = "manage-ujian-exam-online";
    protected $model = "\\Model\\Ujian\\Exam";

    /**
     * List all object in a ExamOnline model
     * May support search and paging. But not sure.
     *
     * @param object $f3 FatFree Object
     * @return void
     */
    public function get_index($f3)
    {
        // echo "Here";
        $this->permission_check_local_ldap($this->permissionPrefix, AclItem::READ);
        // echo "Pass permission check";
        $model = new $this->model;

        if (in_array("deleted_on", $model->fields())) {
            $models = $model->find(["online = ? AND deleted_on = ?", 1, null]);
        } else {
            $models = $model->find(["online = ? ", 1]);
        }

        if ($models === false) {
            // var_dump(["here"]);
            $models = [];
        } else {
            $online_user = OnlineUser::getFromHTTPHeader();


            if ($online_user) {

                //Return only required fields
                $model->fields([
                    //Lecturer data
                    "lecturers.password",
                    "lecturers.exams",
                    "lecturers.participating_exams",
                    "lecturers.acl",
                    "lecturers.email",
                    "lecturers.deleted_on",
                    "lecturers.created_on",
                    "lecturers.updated_on",

                    //Students data
                    "online_participants.password",
                    "online_participants.exams",
                    "online_participants.acl",
                    "online_participants.email",
                    "online_participants.participating_exams",
                    "online_participants.deleted_on",
                    "online_participants.created_on",
                    "online_participants.updated_on",
                ], true);
                $model->has('lecturers', ["uuid = ?", $online_user->uuid]);
                $models = $model->find(["deleted_on = ?", null]);
            } else {
                $models = $model->find(["online = ? and deleted_on = ?", 1, null]);
            }


            // $model = $model->find();



            if ($models === false) {
                $models = [];
            } else {
                $models = $models->castAll();
            }
        }
        return \View\Api::success($models);
    }

    public function get_item_detail($f3)
    {
        // permission checkpoint
        $this->permission_check_local_ldap($this->permissionPrefix, AclItem::READ);

        // id from param must be exists
        $validator = v::key("id", v::notOptional()->mustExists($this->model, 'id', 'deleted_on'));
        try {
            $validator->assert($f3->PARAMS);
        } catch (NestedValidationException $e) {
            throw \Helper\Ruler::transformToError($e);
        }
        $model = new $this->model;
        $model->load(["id = ? AND online = ?", $f3->PARAMS['id'], 1]);

        if ($model->dry()) {
            throw new Error("Object not found", "Object cannot be found", "HTTP404", "Global Validation", 404);
        }
        $model->fields([
            //Lecturer data
            "lecturers.password",
            "lecturers.exams",
            "lecturers.participating_exams",
            "lecturers.acl",
            "lecturers.email",
            "lecturers.deleted_on",
            "lecturers.created_on",
            "lecturers.updated_on",

            //Students data
            "online_participants.password",
            "online_participants.exams",
            "online_participants.acl",
            "online_participants.email",
            "online_participants.participating_exams",
            "online_participants.deleted_on",
            "online_participants.created_on",
            "online_participants.updated_on",
        ], true);

        return \View\Api::success($model->cast());
    }

    /**
     * ##############################################################
     * 
     * ONLINE EXAM CREATION, ADD LECTURER AS COLLABORATOR, ADD STUDENT AS PARTICIPANT, ADD EXAM RESOURCE
     * 
     *  ##############################################################
     */


    /**
     * Create an item with given object.
     *
     * @param object $f3 FatFree Object
     * @return void
     */
    public function  post_index($f3)
    {
        $this->permission_check_local_ldap($this->permissionPrefix, AclItem::CREATE);

        $examObj = [];

        try {
            $model = new $this->model;
            $model->_validate($f3->POST);
            $model->copyfromwithfilter($f3->POST, true);
            $model->save();
            $examObj = $model->cast();
        } catch (NestedValidationException $e) {
            throw \Helper\Ruler::transformToError($e);
        } catch (InvalidArgumentException $e) {
            throw new Error(
                "Bad Request",
                $e->getMessage(),
                "400",
                "Precheck Validation",
                405
            );
        }

        return \View\Api::success($examObj);
    }


    /**
     * Add lecturer by UUID for exam ID.
     * Request include exam id as parameter in url
     * Request body contain UUID of lecturers tha is going to collaborate in exam
     */
    public function post_item_populate_lecturer($f3)
    {
        $this->permission_check_local_ldap($this->permissionPrefix, AclItem::UPDATE);
        $exam = parent::getMentionedItem($f3);

        $currentLecturer = OnlineUser::getFromHTTPHeader();

        //This is logger, currently skipping it

        try {
            $validator = v::key("lecturers", v::optional(v::alpha())->each(v::mustExists("\\Model\\System\\OnlineUser", "uuid", "lecturers.deleted_on")));
            $validator->validate($f3->POST);

            //Get the lecturers data from request
            $lecturers = $f3->POST["lecturers"];
            // print($lecturers[0]);

            //Check if current lecturer included in lecturer list
            //If not include current lecturer to the list
            if ($currentLecturer && !in_array($currentLecturer->uuid, $lecturers)) {
                array_push($lecturers, $currentLecturer->uuid);
            }


            try {
                $lecturers = array_map(function ($lecturerUUID) {
                    $isValidUUID = Uuid::isValid($lecturerUUID);
                    if (!$isValidUUID) {
                        //UUID is not valid, throw an error
                        throw new InvalidArgumentException("Bad Request on populate_lecturer!");
                    }

                    $lecturer = new OnlineUser();
                    $lecturer->load(["uuid = ?", $lecturerUUID]);
                    return $lecturer->id;
                }, $lecturers);
            } catch (NotParseable $e) {
                throw new InvalidArgumentException("Bad Request on populate_lecturer! ERR: " . $e->getMessage());
            }

            if ($exam->dry()) {
                throw new InvalidArgumentException("Bad Request on populate_lecturer! ERR: " . "Exam is dry!");
            }

            $exam->lecturers = $lecturers;
            $exam->save();
            $exam->fields([
                "lecturers.password",
                "lecturers.exams",
                "lecturers.acl",
                "lecturers.email",
                "lecturers.username",
                "lecturers.deleted_on",
                "lecturers.created_on",
                "lecturers.updated_on",

                //Students data
                "online_participants.password",
                "online_participants.exams",
                "online_participants.acl",
                "online_participants.email",
                "online_participants.participating_exams",
                "online_participants.deleted_on",
                "online_participants.created_on",
                "online_participants.updated_on",
            ], true);
            $exam->load(["id = ?", $exam->id]);

            $exam = $exam->cast();
        } catch (NestedValidationException $e) {
            throw \Helper\Ruler::transformToError($e);
        } catch (InvalidArgumentException $e) {
            throw new Error("Invalid Input", $e->getMessage(), "X400", "lecturers.Exception", 400);
        }

        return \View\Api::success($exam);
    }

    /**
     * Add student by UUID for exam ID.
     * Request include exam id as parameter in url
     * Request body contain UUID of students that is going to participate in exam
     */
    public function post_item_populate_student($f3)
    {
        //Check for login and permission
        $this->permission_check_local_ldap($this->permissionPrefix, AclItem::UPDATE);
        //Get exam id from parameter
        $exam = parent::getMentionedItem($f3);

        //Validating request body
        try {
            //Validating request body
            $validator = v::key("participants", v::notOptional()->each(v::regex("/^[0-9]{10}$/s")));
            $validator->validate($f3->POST);

            //Get participants data from header
            $participants = $f3->POST["participants"];
            try {
                $participants = array_map(function ($par) {
                    $info = \Chez14\NpmParser\Solver::getInfo($par);
                    return [$par, $info];
                }, $participants);

                // print_r($participants);
            } catch (NotParseable $e) {
                throw new InvalidArgumentException("There's invalid participant NPM. Please recheck! ERR: " . $e->getMessage());
            }

            shuffle($participants);

            //Acl to assign to new participant
            $acl = new Acl();
            $acl->load(['name LIKE ? AND deleted_on = ?', "student", null]);

            foreach ($participants as $par) {
                $transpiler = $f3->get("NPM_TRANSPILE");
                $username = substr($par[1]['enrollment_year'], 2) . $par[1]['no_urut'];

                if (array_key_exists($par[1]["prodi_id"], $transpiler)) {
                    $username = $transpiler[$par[1]['prodi_id']] . $username;
                } else {
                    throw new InvalidArgumentException("Unknown prodi_id " . $par[1]['prodi_id'] . " from " . $par[0]);
                }

                $participant = new OnlineUser();
                $participant->load(["username LIKE ?", $username]);

                if ($participant->dry()) {
                    //Participant has never been logging in to the system
                    //Registering participant
                    $participant->copyfrom([
                        'uuid' => Uuid::uuid4(),
                        'username' => $username,
                        "email" => $par[0] . '@student.unpar.ac.id',
                        'acl' => ($acl->dry() ? 0 : $acl->id),
                        "participating_exams" => [$exam->_id],
                    ]);

                    $participant->save();
                } else {
                    //Participant has logged in to the system before

                    $examsList = array_map(function ($data) {
                        return $data->id;
                    }, ($participant->participating_exams ? $participant->participating_exams->castAll() : []));


                    array_push($examsList, $exam->id);

                    $participant->participating_exams = $examsList;

                    $participant->save();
                }
            }
        } catch (NestedValidationException $e) {
            throw \Helper\Ruler::transformToError($e);
        }

        $exam->load(["id = ?", $exam->id]);
        $exam->fields([
            "lecturers.password",
            "lecturers.exams",
            "lecturers.acl",
            "lecturers.email",
            "lecturers.username",
            "lecturers.deleted_on",
            "lecturers.created_on",
            "lecturers.updated_on",

            //Students data
            "online_participants.password",
            "online_participants.exams",
            "online_participants.acl",
            "online_participants.email",
            "online_participants.participating_exams",
            "online_participants.deleted_on",
            "online_participants.created_on",
            "online_participants.updated_on",
        ], true);
        $exam = $exam->cast();

        // GENERATE THINGS.
        return \View\Api::success($exam);
    }

    /**
     * Add resource for exam
     * Request body contains FormData of file that is going to be added as resource
     */
    public function post_item_populate_resource($f3)
    {
        //Check for login and permission
        $this->permission_check_local_ldap($this->permissionPrefix, AclItem::UPDATE);
        //Get exam id from parameter (include validation)
        $exam = parent::getMentionedItem($f3);

        try {
            $web = \Web::instance();
            $overwrite = true;
            $slug = true;
            $files = $web->receive(function ($file, $formFieldName) {
                // maybe you want to check the file size
                if ($file['size'] > (20 * 1024 * 1024)) // if bigger than 2 MB
                    return false; // this file is not valid, return false will skip moving it

                // everything went fine, hurray!
                return true; // allows the file to be moved from php tmp dir to your defined upload dir
            }, $overwrite, $slug);

            if (count($files) > 1) {
                throw new \Model\Error(
                    "Bad news",
                    "Given resource is not singular. Send one-by-one please.",
                    "ES05"
                );
            }
            $resource = new \Model\Ujian\Online\ExamResource();

            $file = array_keys($files)[0];

            if ($file && is_file($file)) {
                $resource = $resource->submit($file, $exam);
            } else {
                throw new Error("File not received on server", "File not received on server", "X400", "lecturers.Exception", 400);
            }
        } catch (NestedValidationException $e) {
            throw \Helper\Ruler::transformToError($e);
        } catch (InvalidArgumentException $e) {
            throw new Error("Invalid Input", $e->getMessage(), "X400", "lecturers.Exception", 400);
        }

        // GENERATE THINGS.

        $exam->load(["id = ?", $exam->id]);
        $exam->fields([
            "lecturers.password",
            "lecturers.exams",
            "lecturers.acl",
            "lecturers.email",
            "lecturers.username",
            "lecturers.participating_exams",
            "lecturers.exam_submission",
            "lecturers.deleted_on",
            "lecturers.created_on",
            "lecturers.updated_on",

            //Students data
            "online_participants.password",
            "online_participants.exams",
            "online_participants.acl",
            "online_participants.email",
            "online_participants.participating_exams",
            "online_participants.exam_submission",
            "online_participants.deleted_on",
            "online_participants.created_on",
            "online_participants.updated_on",
        ], true);
        $exam = $exam->cast();

        return \View\Api::success($exam);
    }


    /**
     * ##############################################################
     * 
     * ONLINE EXAM GET NOTIFICATION BY EXAM ID AND CREATE NEW NOTIFICATION FOR EXAM ID
     * 
     *  ##############################################################
     */

    /**
     * Get all notification for exam ID
     */
    public function get_item_notifications($f3)
    {
        //Check for login and permission
        $this->permission_check_local_ldap($this->permissionPrefix, AclItem::READ);

        //Get exam id from parameter (include validation)
        $exam = parent::getMentionedItem($f3);

        //Get all notifications for current exam
        $notification = new NotificationOnline();
        $notification->fields([
            "title",
            "description",
            "extras",
            "type",
            "created_on",
            "updated_on",
            "deleted_on",
            "participants.uuid",
            "participants.username",
            "participants.display_name",
        ]);
        $notification->has("exam", ["id = ?", $exam->id]);
        $notifications = $notification->find();

        if (!$notifications) {
            $notifications = [];
        }


        return \View\Api::success(array_map(function ($data) {
            // $data->fields(["exam"], true);
            $result = $data->cast(null, null, false);
            unset($result["exam"]);
            return $result;
        }, (array) $notifications));
        // return \View\Api::success($notifications->castAll());
    }

    /**
     * Adding new custom  notification for participants in an exam
     * 
     */
    public function post_item_notification($f3)
    {
        //Check for login and permission
        $this->permission_check_local_ldap($this->permissionPrefix, AclItem::UPDATE);

        //Get exam id from parameter (include validation)
        $exam = parent::getMentionedItem($f3);

        try {
            //Validating request body
            $validator = v::key("title", v::notOptional()
                ->key("description", v::notOptional())->key("type", v::notOptional())
                ->key("participants", v::notOptional())->each(v::mustExists("\\Model\\System\\OnlineUser", "uuid", "deleted_on")));
            $validator->validate($f3->POST);

            //Get participants data from header
            $participants = $f3->POST["participants"];
            $title = $f3->POST["title"];
            $description = $f3->POST["description"];
            $type = $f3->POST["type"];

            // return \View\Api::success([$participants, $title, $description, $type]);
            if (is_array($participants) && sizeof($participants)) {
                // $participants . map();

                // $participants = array_map(function ($par) {
                //     $info = \Chez14\NpmParser\Solver::getInfo($par);
                //     return [$par, $info];
                // }, $participants);

                $parIDS = [];

                foreach ($participants as $key => $uuid) {
                    // print_r($value);
                    if (Uuid::isValid($uuid)) {
                        $user = new OnlineUser();
                        $user->load(["uuid LIKE ?", $uuid]);

                        if (!$user->dry()) {
                            array_push($parIDS, $user->_id);
                        }
                    }
                }

                $notification = new NotificationOnline();
                $notification->title = $title;
                $notification->description = $description;
                $notification->type = $type;
                $notification->participants = $parIDS;
                $notification->exam = $exam->_id;

                $notification->save();

                return \View\Api::success($notification->cast());
            }
        } catch (Exception $e) {
            throw new Error("Something unexpected happened!", 500, "Something unexpected happened!", 500);
        }

        return \View\Api::success($exam->cast());
    }



    /**
     * ##############################################################
     * 
     * ONLINE EXAM TIMER START, STOP, RESET 
     * 
     *  ##############################################################
     */

    /**
     * Start an exam
     */
    function post_item_start($f3)
    {
        //Check for login and permission
        $this->permission_check_local_ldap($this->permissionPrefix, AclItem::UPDATE);

        //Get exam id from parameter (include validation)
        $exam = parent::getMentionedItem($f3);

        $exam->fields([
            "lecturers.password",
            "lecturers.exams",
            "lecturers.acl",
            "lecturers.email",
            "lecturers.username",
            "lecturers.participating_exams",
            "lecturers.exam_submission",
            "lecturers.deleted_on",
            "lecturers.created_on",
            "lecturers.updated_on",

            //Students data
            "online_participants.password",
            "online_participants.exams",
            "online_participants.acl",
            "online_participants.email",
            "online_participants.participating_exams",
            "online_participants.exam_submission",
            "online_participants.deleted_on",
            "online_participants.created_on",
            "online_participants.updated_on",
        ], true);

        if ($exam->time_opened === null) {
            $exam->time_opened = time();
            $exam->time_ended = strtotime("+" . $exam->time_duration . " seconds");
            $exam->save();

            $logger = $exam->getLoggerInstance();
            $logger->notice("Timer start via " . $f3->IP);
        } else {
            //Exam has already been started! Need to stop it first!
            throw new Error("Exam has already been started!", 400, "Bad Request", 400);
        }

        return \View\Api::success($exam->cast());
    }

    /**
     * Stop an exam
     */
    function post_item_stop($f3)
    {
        //Check for login and permission
        $this->permission_check_local_ldap($this->permissionPrefix, AclItem::UPDATE);

        //Get exam id from parameter (include validation)
        $exam = parent::getMentionedItem($f3);
        $exam->fields([
            "lecturers.password",
            "lecturers.exams",
            "lecturers.acl",
            "lecturers.email",
            "lecturers.username",
            "lecturers.participating_exams",
            "lecturers.exam_submission",
            "lecturers.deleted_on",
            "lecturers.created_on",
            "lecturers.updated_on",

            //Students data
            "online_participants.password",
            "online_participants.exams",
            "online_participants.acl",
            "online_participants.email",
            "online_participants.participating_exams",
            "online_participants.exam_submission",
            "online_participants.deleted_on",
            "online_participants.created_on",
            "online_participants.updated_on",
        ], true);
        //Exam has already been started, so it can be stopped
        if ($exam->time_ended !== null && $exam->time_opened !== null) {
            $exam->time_ended = time();
            $exam->save();

            //Set resource token valid_until to this time -1 minutes

            $logger = $exam->getLoggerInstance();
            $logger->notice("Timer start via " . $f3->IP);
        } else {
            //Exam has not  been started! Need to start it first!
            throw new Error("Exam has not been started!", 400, "Bad Request", 400);
        }

        return \View\Api::success($exam->cast());
    }


    /**
     * Reset an exam time
     */
    function post_item_reset($f3)
    {
        //Check for login and permission
        $this->permission_check_local_ldap($this->permissionPrefix, AclItem::UPDATE);

        //Get exam id from parameter (include validation)
        $exam = parent::getMentionedItem($f3);

        $exam->fields([
            "lecturers.password",
            "lecturers.exams",
            "lecturers.acl",
            "lecturers.email",
            "lecturers.username",
            "lecturers.participating_exams",
            "lecturers.exam_submission",
            "lecturers.deleted_on",
            "lecturers.created_on",
            "lecturers.updated_on",

            //Students data
            "online_participants.password",
            "online_participants.exams",
            "online_participants.acl",
            "online_participants.email",
            "online_participants.participating_exams",
            "online_participants.exam_submission",
            "online_participants.deleted_on",
            "online_participants.created_on",
            "online_participants.updated_on",
        ], true);

        $exam->time_start = time();
        $exam->time_opened = null;
        $exam->time_ended = null;
        $exam->save();

        $resources = $exam->resource;

        //Reset resource token validity
        foreach ($resources as $resource) {
            $updated = new ResourceToken();
            $updated->load(["_id  = ?", $resource->token->_id]);

            if ($updated->loaded()) {
                //Get exam start time
                $startTime = DateTime::createFromFormat("Y-m-d H:i:s", $exam->time_start);

                //Create interval 10 minutes before exam start
                $downloadInterval  = new DateInterval('PT10M');

                //Adding interval to start time
                $validFromTime = $startTime->sub($downloadInterval);

                //assign new time as valid_from time
                $updated->valid_from = $validFromTime->format("Y-m-d H:i:s");

                //Create interval 30 minutes after exam start
                $limitDownloadInterval = new DateInterval("PT" . ExamResource::TOKEN_VALID_DURATION . "S");

                //Get exam start time
                $endTime = DateTime::createFromFormat("Y-m-d H:i:s", $exam->time_start);

                //Adding interval to end time
                $validUntilTime = $endTime->add($limitDownloadInterval);

                //assign new time as valid_from time
                $updated->valid_until = $validUntilTime->format("Y-m-d H:i:s");

                //Save the model to database
                $updated->save();
            }
        }


        return \View\Api::success($exam->cast());
    }


    function delete_item($f3, $directDelete = true)
    {
        $online_user = OnlineUser::getFromHTTPHeader();
        if ($online_user) {
            $this->permission_check_ldap($this->permissionPrefix, AclItem::DELETE);
            $examToDelete = $this->getMentionedItem($f3);
            $exam = new $this->model;
            $exam->has("lecturers", ["uuid LIKE ?", $online_user->uuid]);
            $exam->load(["id = ?", $examToDelete->_id]);

            if ($exam->dry()) {
                throw new Error("You don't have access to this exam!", 403, "You don't have access to this exam", 403);
            } else {
                parent::delete_item($f3, $directDelete);
            }
        } else {
            $adminUser = User::getFromHTTPHeader();
            if ($adminUser) {
                $this->permission_check($this->permissionPrefix, AclItem::DELETE);
                parent::delete_item($f3, $directDelete);
            }
        }
    }

    function put_item($f3)
    {
        $this->permission_check_local_ldap($this->permissionPrefix, AclItem::UPDATE);

        //Get exam id from parameter (include validation)
        $examToUpdate = parent::getMentionedItem($f3);

        //Get logged in online user
        $online_user = OnlineUser::getFromHTTPHeader();

        //If online user has logged in
        if ($online_user) {

            //Check if the exam to be modified has the logged in user as collaborator
            $exam = new $this->model;
            $exam->has("lecturers", ["uuid LIKE ?", $online_user->uuid]);
            $exam->load(["id = ?", $examToUpdate->_id]);

            //If logged in user is not a collaborator, throw error
            if ($exam->dry()) {
                throw new Error("You don't have access to this exam!", 403, "You don't have access to this exam", 403);
            } else {

                //If lecturers / collaborator is included in request
                if ($f3->POST["lecturers"]) {

                    //Get lecturers UUID array from request
                    $lecturers = $f3->POST["lecturers"];

                    $payload = $f3->POST;
                    unset($payload["lecturers"]);


                    //FOr each UUID specified in POST.lecturers, check if UUID matching with existing user (lecturer)
                    $lecturersId = array_map(function ($id) {
                        $currLecturer = new OnlineUser();

                        $currLecturer->load(["uuid LIKE ?", $id]);

                        if (!$currLecturer->dry()) {
                            return $currLecturer->_id;
                        }
                    }, $lecturers);


                    $obj = [];
                    try {
                        $model = new $this->model;
                        $model->_validate($payload);
                        $this->globalFilterTrigger($model);
                        $model->load(["id=?", $f3->PARAMS['id']]);
                        if ($model->dry()) {
                            throw new InvalidArgumentException("Global validation failed");
                        }
                        $model->copyfromwithfilter(array_diff_key($payload, array_reverse(['id', '_id'])), true);
                        $model->lecturers = $lecturersId;
                        $model->save();
                        $obj = $model->cast();
                    } catch (NestedValidationException $e) {
                        throw \Helper\Ruler::transformToError($e);
                    } catch (InvalidArgumentException $e) {
                        throw new Error(
                            "Bad Request",
                            $e->getMessage(),
                            "400",
                            "Precheck Validation",
                            405
                        );
                    }
                    return \View\Api::success($obj);
                } else {
                    parent::put_item($f3);
                }
            }
        } else {
            $adminUser = User::getFromHTTPHeader();
            if ($adminUser) {

                if ($f3->POST["lecturers"]) {

                    $lecturers = $f3->POST["lecturers"];

                    $payload = $f3->POST;
                    unset($payload["lecturers"]);

                    $lecturersId = array_map(function ($id) {
                        $currLecturer = new OnlineUser();

                        $currLecturer->load(["uuid LIKE ?", $id]);

                        if (!$currLecturer->dry()) {
                            return $currLecturer->_id;
                        }
                    }, $lecturers);


                    $obj = [];
                    try {
                        $model = new $this->model;
                        $model->_validate($payload);
                        $this->globalFilterTrigger($model);
                        $model->load(["id=?", $f3->PARAMS['id']]);
                        if ($model->dry()) {
                            throw new InvalidArgumentException("Global validation failed");
                        }
                        $model->copyfromwithfilter(array_diff_key($payload, array_reverse(['id', '_id'])), true);
                        $model->lecturers = $lecturersId;
                        $model->save();
                        $obj = $model->cast();
                    } catch (NestedValidationException $e) {
                        throw \Helper\Ruler::transformToError($e);
                    } catch (InvalidArgumentException $e) {
                        throw new Error(
                            "Bad Request",
                            $e->getMessage(),
                            "400",
                            "Precheck Validation",
                            405
                        );
                    }
                    return \View\Api::success($obj);
                } else {
                    // var_dump($f3->POST);
                    // echo $f3->POST["time_duration"];
                    parent::put_item($f3);
                }
            }
        }
    }

    /**
     * Zip answers for current exam and return it as attachment
     *
     * @param \Base $f3
     * @return void
     */
    public function get_item_answers(\Base $f3)
    {
        // Check if user is logged in and has permission required.
        $this->permission_check_local_ldap($this->permissionPrefix, AclItem::READ);

        //Get exam info based on id from request
        $ujian = parent::getMentionedItem($f3);

        //If exam id specified exist, process will be continued
        if ($ujian) {

            $online_user = OnlineUser::getFromHTTPHeader($f3);
            $adminUser = User::getFromHTTPHeader($f3);

            if ($online_user) {
                //Logged in user is student or teacher (OnlineUser)    

                //Check if logged in user has acl named "lecturer"
                $online_user->has("acl", ["name LIKE ?", "lecturer"]);
                //Check if logged in user can managed exam specified
                $online_user->has("exams", ["id = ? ", $ujian->_id]);
                //Load the user with UUID like the logged in user
                $online_user->load(["UUID LIKE ?", $online_user->uuid]);

                if ($online_user->loaded()) {
                    //User is a lecturer and has right to manage the exam
                    //And has uuid mathing the logged in user
                    $this->zipExam($f3, $ujian);
                }
            } else if (!$online_user && $adminUser) {
                //Logged in user is admin
                $this->zipExam($f3, $ujian);
            }
        } else {
            //Exam with id specified is not exist
            throw new Error(
                "Bad Request",
                "Exam id specified is invalid",
                "400",
                "Bad Request",
                400
            );
        }
    }

    protected function zipExam($f3, $ujian)
    {

        $zipname = AnswerZipper::zipExamOnline($ujian);
        $filename = AnswerZipper::getProperFilename($ujian);

        $filename .= ".zip";

        $logger = $ujian->getLoggerInstance();
        $logger->warn("Exam online answers download request via " . $f3->IP);

        header("X-Filename: $filename");
        \Web::instance()->send(
            $zipname,
            null,
            0,
            true,
            $filename
        );

        if (is_file($zipname)) {
            unlink($zipname);
        }
    }
}
