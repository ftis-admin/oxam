<?php

namespace Controller\Api\ManageOnline;

use Controller\CRUDBase;
use Helper\ExamReport as HelperExamReport;
use Model\System\AclItem;
use View\Api;

class ExamReport extends CRUDBase
{
    protected $permissionPrefix = "manage-ujian-exam-online-report";
    protected $model = "\\Model\\Ujian\\ExamReport";

    public function post_item_forcesend($f3)
    {
        $this->permission_check_local_ldap($this->permissionPrefix, AclItem::UPDATE);
        $examreport = parent::getMentionedItem($f3);

        $log = $examreport->exam->getLoggerInstance();
        $log->notice("[MANUAL] Autoreport for online exam is sent to " . $examreport->tos . " with id ExamReport#" . $examreport->_id);

        // push validity periode of the token
        // just to make sure things gone right
        $examreport->pushValidity();
        $examreport->sent_on = time();
        $examreport->save();

        HelperExamReport::sendoutOnline($examreport);

        return Api::success([]);
    }
}
