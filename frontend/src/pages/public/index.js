import Forbidden from "./Forbidden";
import NotFound from "./NotFound";

export {
    Forbidden,
    NotFound
};
