import React from 'react';
import { Route, Switch } from 'react-router-dom';
import AdminNavbar from '~/components/admin/navbar/AdminNavbar';

import ExamOnlineCreate from "./exam/create/Create";
import Detail from './exam/detail/Detail';
import Screen from './exam/detail/screen/Screen';
import Edit from './exam/edit/Edit';
import Lecturer from './Lecturer';

const LecturerIndex = ({ match }) => {
    return (
        <React.Fragment>
            <AdminNavbar />
            <Switch>
                <Route exact path={match.path} component={Lecturer} />
                <Route exact path={match.path + "/exam"} component={Lecturer} />
                <Route path={match.path + "/exam/new"} component={ExamOnlineCreate} />
                <Route path={match.path + "/exam/:id/detail"} component={Detail} />
                <Route path={match.path + "/exam/:id/edit"} component={Edit} />
                <Route path={match.path + "/exam/:id/screen"} component={Screen} />
                {/* <Route path={match.path + "/manage"} component={Manage} /> } */}
            </Switch>
        </React.Fragment>
    )
}

export default LecturerIndex; 