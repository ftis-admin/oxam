import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { observer } from 'mobx-react';
import moment from "moment";
import { useEffect, useState } from 'react';
import { Else, If, Then } from 'react-if';
import { Link } from "react-router-dom";
import { Modal, ModalBody, ModalFooter, ModalHeader, Spinner } from 'reactstrap';
import Badge from "reactstrap/lib/Badge";
import Button from "reactstrap/lib/Button";
import Col from "reactstrap/lib/Col";
import Container from "reactstrap/lib/Container";
import Row from "reactstrap/lib/Row";
import Table from "reactstrap/lib/Table";
import { useAdminStore } from '~/components/use-store';

function Exam() {
  const adminStore = useAdminStore();
  useEffect(() => {
    adminStore.examFetch();
    return () => { };
  }, [adminStore])

  const [prepareDelete, setPrepareDelete] = useState({})
  const [isDeleting, setIsDeleting] = useState(false);

  function handleDelete() {
    setIsDeleting(true);
    adminStore.examDelete(prepareDelete._id).then(() => {
      setIsDeleting(false);
      setPrepareDelete({});
    })
  }
  return (
    <Container>
      <Row className="mt-4">
        <Col>
          <h1>Ujian</h1>
        </Col>
        <Col>
          <div className="text-right">
            <Button color="primary" size="lg" tag={Link} to="/admin/exam/new">Create New</Button>
          </div>
        </Col>
      </Row>
      <Row>
        <Col>
          <Table striped>
            <thead>
              <tr>
                <th>Lecture Info</th>
                <th>Shift</th>
                <th colSpan={2}>Duration (Start-End)</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {adminStore.exams.map((exam, i) =>
                <tr key={i}>
                  <td>
                    <b>{(exam.lecture || {}).name} ({(exam.lecture || {}).lecture_code})</b> <br />
                    {(exam.lecture_period || {}).period_code} <Badge color={parseInt(exam.uts) ? (parseInt(exam.uts) === 1 ? "success" : "primary") : "info"}>{parseInt(exam.uts) ? (parseInt(exam.uts) === 1 ? "UTS" : "QUIZ") : "UAS"}</Badge>
                    <Badge className="mx-2" color={exam.online ? "warning" : "secondary"}>{exam.online ? "ONLINE" : "OFFLINE"}</Badge>
                  </td>
                  <td>
                    <p className="h3">
                      <If condition={exam.shift === null}>
                        <Then>-</Then>
                        <Else>{exam.shift}</Else>
                      </If>
                    </p>
                  </td>
                  <td>
                    {exam.time_start} - {exam.time_ended}
                  </td>
                  <td>
                    Durasi: {exam.time_duration / 60} Menit<br />
                    Dimulai pada: {exam.time_opened || "(belum di mulai)"}
                  </td>
                  <td className="text-right">
                    <Button className="mx-2 d-none d-md-block" color="warning" tag={Link} to={`/admin/exam/${exam._id}/detail`}>Lihat</Button>
                    <Button className="mx-2 d-block d-md-none" color="warning" tag={Link} to={`/admin/exam/${exam._id}/minipanel`}>Lihat</Button>
                    <Button className="mx-2" color="danger" onClick={() => setPrepareDelete(exam)}>Hapus</Button>
                  </td>
                </tr>
              )}
            </tbody>
          </Table>
        </Col>
      </Row>

      <Modal isOpen={!!(prepareDelete._id)} toggle={() => setPrepareDelete({})} backdrop>
        <ModalHeader toggle={() => setPrepareDelete({})}>Konfirmasi Hapus</ModalHeader>
        <ModalBody>
          <h4>Are you sure to delete this exam?</h4>
          <h3>#{prepareDelete._id} <Badge color={prepareDelete.uts ? "info" : "success"}>{prepareDelete.uts ? "UTS" : "UAS"}</Badge> {prepareDelete.lecture?.name} ({(prepareDelete.lecture || {}).lecture_code})</h3>
          <p className="lead"> {(prepareDelete.shift === 0) ? "Shiftless" : ("Shift " + prepareDelete.shift)} | {moment(prepareDelete.time_start).format("LLLL")} | {prepareDelete.duration / 60} Menit.</p>
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={() => setPrepareDelete({})}>Cancel</Button>{' '}
          <Button color="danger" onClick={handleDelete}><FontAwesomeIcon icon={faTrash} /> Hapus{isDeleting ? <Spinner as="span"
            animation="border"
            size="sm"
            role="status"
            aria-hidden="true"
          /> : ""

          }</Button>
        </ModalFooter>
      </Modal>

    </Container >)
}


export default observer(Exam)