import { makeObservable, observable } from "mobx";


class ComputerLocationStore {
    locationComputer = [];

    constructor() {
        makeObservable(this, {
            locationComputer: observable
        });
    }
}



export default ComputerLocationStore;