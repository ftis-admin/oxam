export const convertNumberToIP = (subnetStart, number) => {
    if (subnetStart && number) {
        let octets = subnetStart.split(".");
        if (octets.length === 4) {
            let newNumber = Number(octets[3]) + Number(number);
            octets[3] = newNumber;

            return octets.join(".");
        }
    }
}


export const convertNumberTODNS = (nameAlias, number) => {
    let dns_reverse = nameAlias + "-" + (number >= 10 ? number : "0" + number);
    return dns_reverse;
}