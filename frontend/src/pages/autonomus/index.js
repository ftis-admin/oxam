import React from 'react'
import { Route, Switch } from 'react-router-dom'
import ExamExtractIndex from './exam-extract'
import ExamOnlineExtractIndex from "./exam-online-extract"

function AutonomusIndex({ match }) {
    return (
        <Switch>
            <Route path={match.path + "/exam-online-extract/:token"} component={ExamOnlineExtractIndex} />
            <Route path={match.path + "/exam-extract/:token"} component={ExamExtractIndex} />
        </Switch>
    )
}

export default AutonomusIndex
