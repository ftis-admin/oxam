import { observer } from 'mobx-react';
import { Col, Row } from 'reactstrap';
import Computer from './computer';

function ComputersContainer({ computers = [], editable = true, onComputerClick = () => { } }) {

    // PROCCESSOR
    let rows = {};
    computers.map(comp => {
        if (!rows[comp.pos_y]) {
            rows[comp.pos_y] = [];
        }
        rows[comp.pos_y].push(comp);
        return rows;
    })

    // reurut:
    let comp = Object.values(rows).sort((a, b) => (a[0].pos_y - b[0].pos_y)).map((el) => el.sort((a, b) => (a.pos_x - b.pos_x)));
    return (
        <>
            {comp.map((el, i) => <Row key={i}>
                {el.map((com, c, arr) => {
                    let offset = 0;

                    if (c === 0 && com.pos_x > 1) {
                        offset = com.pos_x - 1;
                    } else if (c > 0 && com.pos_x - arr[c - 1].pos_x !== 1) {
                        offset = com.pos_x - arr[c - 1].pos_x - 1;
                    }

                    return <Col xs={{ size: 2, offset: (offset * 2) }} key={c}>
                        <Computer
                            setting={com}
                            selectable={editable}
                            selected={!!com.selected}
                            onClick={() => onComputerClick(com)}
                        />
                    </Col>
                })}
            </Row>)}
        </>
    )
}

export default observer(ComputersContainer);
