import { checkPermission } from "./CheckPermission";
import { makeId } from "./Utils";

export {
    makeId,
    checkPermission
};

